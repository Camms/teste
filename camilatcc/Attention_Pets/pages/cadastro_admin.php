<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Título -->
  <title>Attention Pets</title>
  <link rel="icon" type="imagem/png" href="imagens/logos/icon.png" />

  <!-- Estilos -->
	<link href="estilo.css" rel="stylesheet">
   

  <style>
body {
      background-color: #EDEDED;
      font-family: "Lato", sans-serif;
    }

    * {
      /*       border: 1px red solid; */

    }

    body {
      overflow-x: hidden;
    }

    .list-group-item {
      font-family: 'Comfortaa', cursive;
      font-size: 13px;
      text-indent: 12px;
      color: black;
    }

    #sidebar-wrapper {
      margin-left: -15rem;
      -webkit-transition: margin .25s ease-out;
      -moz-transition: margin .25s ease-out;
      -o-transition: margin .25s ease-out;
      transition: margin .25s ease-out;
    }

    #sidebar-wrapper a {
      border-radius: 2px;
      text-decoration: none;
      font-size: 13px;
      display: block;
      transition: 0.3s;
    }

    #sidebar-wrapper .sidebar-heading {
      padding: 0.875rem 1.25rem;
      font-size: 1.2rem;

    }

    #sidebar-wrapper .list-group {
      width: 15rem;
    }

    #page-content-wrapper {
      min-width: 100vw;
    }

    #wrapper.toggled #sidebar-wrapper {
      margin-left: 0;
    }
      
@media (min-width: 768px) {
  #sidebar-wrapper {
    margin-left: 0;
  }

  #page-content-wrapper {
    min-width: 0;
    width: 100%;
  }

  #wrapper.toggled #sidebar-wrapper {
    margin-left: -15rem;
  }
}
.bg-light {
  background-color: #f8f9fa !important;
}

a.bg-light:hover, a.bg-light:focus,
button.bg-light:hover,
button.bg-light:focus {
  background-color: #dae0e5 !important;
}
.card > .list-group:first-child .list-group-item:first-child {
  border-top-left-radius: 0.25rem;
  border-top-right-radius: 0.25rem;
}

.card > .list-group:last-child .list-group-item:last-child {
  border-bottom-right-radius: 0.25rem;
  border-bottom-left-radius: 0.25rem;
}
.list-group-flush .list-group-item {
  border-right: 0;
  border-left: 0;
  border-radius: 0;
}

.list-group-flush:first-child .list-group-item:first-child {
  border-top: 0;
}

.list-group-flush:last-child .list-group-item:last-child {
  border-bottom: 0;
}    
   .list-group-item-action {
  width: 100%;
  color: #495057;
  text-align: inherit;
}

.list-group-item-action:hover, .list-group-item-action:focus {
  color: #495057;
  text-decoration: none;
  background-color: #f8f9fa;
}

.list-group-item-action:active {
  color: #212529;
  background-color: #e9ecef;
}

.list-group-item {
  position: relative;
  display: block;
  padding: 0.75rem 1.25rem;
  margin-bottom: -1px;
  background-color: #fff;
  border: 1px solid rgba(0, 0, 0, 0.125);
}

.list-group-item:first-child {
  border-top-left-radius: 0.25rem;
  border-top-right-radius: 0.25rem;
}

.list-group-item:last-child {
  margin-bottom: 0;
  border-bottom-right-radius: 0.25rem;
  border-bottom-left-radius: 0.25rem;
}

.list-group-item:hover, .list-group-item:focus {
  z-index: 1;
  text-decoration: none;
}

.list-group-item.disabled, .list-group-item:disabled {
  color: #6c757d;
  background-color: #fff;
}

.list-group-item.active {
  z-index: 2;
  color: #fff;
  background-color: #007bff;
  border-color: #007bff;
}   
      .list-group-item-action {
  width: 100%;
  color: #495057;
  text-align: inherit;
}

.list-group-item-action:hover, .list-group-item-action:focus {
  color: #495057;
  text-decoration: none;
  background-color: #f8f9fa;
}

.list-group-item-action:active {
  color: #212529;
  background-color: #e9ecef;
}
      .card-body {
          width: 500px;
      }
  </style>
</head>

<body id="inicio" style="background-image:url(imagens/teste3.png); ">
  

   <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-bottom: 19px;"><img src="imagens/logos/Logo_Nav_Icon.png" width="200" class="img-fluid"></div>
      <div class="list-group list-group-flush">

        <a href="index.php?id=admin" class="list-group-item list-group-item-action bg-light js-scroll-trigger"><img src="imagens/icons/house.svg" class="mb-2 mt-2" width="30"> Início</a>

        <a href="index.php?id=cadastro_admin" class="list-group-item list-group-item-action bg-light js-scroll-trigger"><img src="imagens/icons/user.svg" width="30" class="mb-2 mt-2"> Cadastrar Usuário</a>

       <?php
        @session_start();
        if (isset($_SESSION['logado'])) {
          $logado = $_SESSION['logado'];
          $idUsuario = $_SESSION['idUsuario'];
          $nome = $_SESSION['nome']; 
          ?>
          <!--INICIO - Sair-->
          <a href='index.php?id=logout' class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/logout.svg" class="mb-2 mt-2" width="30"> Sair</a>
          <!--FIM - Sair-->
        <?php
        }
        ?>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">

        <img src="imagens/icons/menu-button.svg" class="mb-2 mt-2" width="30" id="menu-toggle" style="cursor:pointer;"><div class="text-center text-muted mb-4">
                  
                </div> <h5 style="font-size:20px; text-indent:10px;padding-right:30px;margin-top: 10px;"><b>Bem-vindo(a)</b>, <?php echo $nome ?>  <img src="imagens/icons/pet.svg" width="30"></h5>
      </nav>
      <div>
    </div>
  <!--FIM - Menu-->

  
    <?php
  if(!isset($_POST['conf'])){
 ?>
<div class="login-form">
        <form action="index.php?id=cadastro_admin" method="post">
          <input type="hidden" name="conf" value="1" />
<div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-5 mt-5 mb-5">
            <div class="card bg-secondary shadow border-0"> 
              <div style="padding-right: 20px;padding-left: 20px;padding-bottom: 30px;padding-top: 40px;">
                <div class="text-center text-muted mb-4">
                  <h5><b>Cadastro</b>, é rápido e fácil  <img src="imagens/icons/pet.svg" width="30"></h5>
                </div>
                  
                <form role="form">
                    
                     <!-- Nome -->
                  <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-pencil-alt"></i></span>
                      </div>
                        
                      <input type="text" class="form-control" name="nome" placeholder="Nome Completo" required="required" style="text-indent: 5px;">
                        
                    </div>
                  </div>
                    <!-- Usuário -->
                    <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                      </div>
                        
                      <input type="text" class="form-control" name="usuario" placeholder="Usuario" required="required" style="text-indent: 5px;">
                        
                    </div>
                  </div>
                    
                    <!-- Email --> 
                  <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                      </div>
                        
                      <input type="email" class="form-control" name="email" placeholder="E-mail" required="required" style="text-indent: 5px;">
                        
                    </div>
                  </div>
                    
                     <!-- Senha -->
                  <div class="form-group">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                      </div>
                        
                      <input type="password" class="form-control" name="senha" placeholder="Senha" required="required" style="text-indent: 5px;">				   
                    </div>
                  </div>
                    
                    <div class="form-group">
                  <div class="row my-4">
                    <div class="col-12">
                      <div class="custom-control custom-control-alternative custom-checkbox">
                        <input class="custom-control-input" id="customCheckRegister" type="checkbox" required="required">  
                        <label class="custom-control-label" for="customCheckRegister">
                          <span style="font-family: 'Comfortaa', cursive; font-size:12px">Eu concordo com a
                            <a href="#" style="font-size:12px" >Política de Privacidade</a>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                   </div>
                        
                  <div class="form-group">
                   <button type="submit" class="btn btn-info btn-block">CADASTRAR</button>
                  </div>
                </form>
              </div>
            </div>
        </div>
    </div>  
</div>
</form>
</div>
<?php } else
{
	include_once("funcoes.php");
	$PDO = conectar();
    $nomeLogin = strip_tags($_POST['nome']);
	$usuarioLogin = strip_tags($_POST['usuario']);
	$emailLogin = strip_tags($_POST['email']);
    $senhaLogin = strip_tags($_POST['senha']);
	$sql="INSERT INTO usuarios (nome, usuario, email, senha) VALUES (:nome, :usuario, :email, :senha)";
	$inserir= $PDO->prepare($sql);
    $inserir->bindParam(":nome",$nomeLogin);
	$inserir->bindParam(":usuario",$usuarioLogin);
	$inserir->bindParam(":email",$emailLogin);
    $inserir->bindParam(":senha",$senhaLogin);  
	$inserir->execute();
	$tot_reg = $inserir->rowCount();
	if($tot_reg>0)
	{
  	echo "<script>location.href='index.php?id=admin'</script>";
	}
}
 ?>      


  </div></div>
    <!-- Bootstrap core JavaScript -->
  <script src="assets/jquery/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
    
</body>
</html>