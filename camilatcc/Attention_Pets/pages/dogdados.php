<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Título -->
  <title>Attention Pets</title>
  <link rel="icon" type="imagem/png" href="imagens/logos/icon.png" />

  <!-- Estilos -->
  <link href="estilo.css" rel="stylesheet">


  <style>
    body {
      background-color: #EDEDED;
      font-family: "Lato", sans-serif;
    }

    * {
      /*       border: 1px red solid; */

    }

    body {
      overflow-x: hidden;
    }

    .card,
    img {
        border-radius: 20px;
    }

    .list-group-item {
      font-family: 'Comfortaa', cursive;
      font-size: 13px;
      text-indent: 12px;
      color: black;
    }

    #sidebar-wrapper {
      margin-left: -15rem;
      -webkit-transition: margin .25s ease-out;
      -moz-transition: margin .25s ease-out;
      -o-transition: margin .25s ease-out;
      transition: margin .25s ease-out;
    }

    #sidebar-wrapper a {
      border-radius: 2px;
      text-decoration: none;
      font-size: 13px;
      display: block;
      transition: 0.3s;
    }

    #sidebar-wrapper .sidebar-heading {
      padding: 0.875rem 1.25rem;
      font-size: 1.2rem;

    }

    #sidebar-wrapper .list-group {
      width: 15rem;
    }

    #page-content-wrapper {
      min-width: 40vw;
    }

    #wrapper.toggled #sidebar-wrapper {
      margin-left: 0;
    }

    .btn-FaleConosco {
      border: 1px solid transparent;
      border-color: #11cdef;
      background-color: #11cdef;
      padding: .375rem .75rem;
      border-radius: .25rem;
      transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
      font-size: .875rem;
    }

    @media (min-width: 1024px) {
      #sidebar-wrapper {
        margin-left: 0;
      }

      #page-content-wrapper {
        min-width: 0;
        width: 100%;
      }

      #wrapper.toggled #sidebar-wrapper {
        margin-left: -15rem;
      }
    }

    .bg-light {
      background-color: #f8f9fa !important;
    }

    a.bg-light:hover,
    a.bg-light:focus,
    button.bg-light:hover,
    button.bg-light:focus {
      background-color: #dae0e5 !important;
    }

    .card>.list-group:first-child .list-group-item:first-child {
      border-top-left-radius: 0.25rem;
      border-top-right-radius: 0.25rem;
    }

    .card>.list-group:last-child .list-group-item:last-child {
      border-bottom-right-radius: 0.25rem;
      border-bottom-left-radius: 0.25rem;
    }

    .list-group-flush .list-group-item {
      border-right: 0;
      border-left: 0;
      border-radius: 0;
    }

    .list-group-flush:first-child .list-group-item:first-child {
      border-top: 0;
    }

    .list-group-flush:last-child .list-group-item:last-child {
      border-bottom: 0;
    }

    .list-group-item-action {
      width: 100%;
      color: #495057;
      text-align: inherit;
    }

    .list-group-item-action:hover,
    .list-group-item-action:focus {
      color: #495057;
      text-decoration: none;
      background-color: #f8f9fa;
    }

    .list-group-item-action:active {
      color: #212529;
      background-color: #e9ecef;
    }

    .list-group-item {
      position: relative;
      display: block;
      padding: 0.75rem 1.25rem;
      margin-bottom: -1px;
      background-color: #fff;
      border: 1px solid rgba(0, 0, 0, 0.125);
    }

    .list-group-item:first-child {
      border-top-left-radius: 0.25rem;
      border-top-right-radius: 0.25rem;
    }

    .list-group-item:last-child {
      margin-bottom: 0;
      border-bottom-right-radius: 0.25rem;
      border-bottom-left-radius: 0.25rem;
    }

    .list-group-item:hover,
    .list-group-item:focus {
      z-index: 1;
      text-decoration: none;
    }

    .list-group-item.disabled,
    .list-group-item:disabled {
      color: #6c757d;
      background-color: #fff;
    }

    .list-group-item.active {
      z-index: 2;
      color: #fff;
      background-color: #007bff;
      border-color: #007bff;
    }

    .list-group-item-action {
      width: 100%;
      color: #495057;
      text-align: inherit;
    }

    .list-group-item-action:hover,
    .list-group-item-action:focus {
      color: #495057;
      text-decoration: none;
      background-color: #f8f9fa;
    }

    .list-group-item-action:active {
      color: #212529;
      background-color: #e9ecef;
    }

    .card-body {
      width: 500px;
    }
  </style>
</head>

<body id="inicio" style="background-image:url(imagens/teste3.png); ">


  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-bottom: 17px;"><img src="imagens/logos/Logo_Nav_Icon.png" width="200" class="img-fluid"></div>
      <div class="list-group list-group-flush">

        <a href="index.php?id=menu" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/house.svg" class="mb-2 mt-2" width="30"> Início</a>

        <a href="index.php?id=cadastro_pets" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/dog.svg" width="30" class="mb-2 mt-2"> Cadastrar Pet</a>

        <a href="index.php?id=menu#faleconosco" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/network.svg" class="mb-2 mt-2" width="30"> Fale Conosco</a>

        <a href="index.php?id=menu#mapa" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/maps-and-flags.svg" class="mb-2 mt-2" width="30"> Mapa</a>

        <?php
        @session_start();
        if (isset($_SESSION['logado'])) {
          ?>
          <!--INICIO - Sair-->
          <a href='index.php?id=logout' class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/logout.svg" class="mb-2 mt-2" width="30"> Sair</a>
          <!--FIM - Sair-->
        <?php
        }
        ?>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <img src="imagens/icons/menu-button.svg" class="mb-2 mt-2" width="30" id="menu-toggle" style="cursor:pointer;">
      </nav>

      <!--FIM - Menu-->

      <script>
        function openNav() {
          document.getElementById("mySidenav").style.width = "250px";
          document.getElementById("main").style.marginLeft = "250px";
        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
          document.getElementById("main").style.marginLeft = "0";
        }
      </script>

      <!--INICIO - Modal Adicionar Dieta -->
      <div class="modal fade" id="AddDieta" tabindex="-1" role="dialog" aria-labelledby="AddDietaLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="AddDietaLabel">Adicionar dieta</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
    
              <div class="modal-body">
                <form method="post">
                  <input type="hidden" name="conf" value="1" />



                  <div class="form-group">
                    <p class="text-muted mb-2">Tipo do alimento</p>
                    <input type="text" class="form-control" name="alimento" placeholder="Exemplo: Ração" required="required">
                  </div>

                  <div class="form-group">
                    <p class="text-muted mb-2">Sabor do alimento</p>
                    <input type="text" class="form-control" name="sabor" placeholder="Exemplo: Carne e Arroz" required="required">
                  </div>

                  <div class="form-group">
                    <p class="text-muted mb-2">Marca</p>
                    <input type="text" class="form-control" name="marca" placeholder="Exemplo: Premier" required="required">
                  </div>




                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-success" name="AddDieta">Adicionar</button>
                </form>
              </div>
            <?php if (isset($_POST['AddDieta'])) {
              include_once("funcoes.php");
              $PDO = conectar();
              $id_animal = $_GET['id_animal'];
              $tipoalimentoDieta = strip_tags($_POST['alimento']);
              $saboralimentoDieta = strip_tags($_POST['sabor']);
              $marcaAlimentoDieta = strip_tags($_POST['marca']);
              $sql7 = "INSERT INTO dieta (alimento, sabor, marca, FK_id_animal) VALUES (:alimento, :sabor, :marca, :FK_id_animal)";
              $inserir = $PDO->prepare($sql7);
              $inserir->bindParam(":alimento", $tipoalimentoDieta);
              $inserir->bindParam(":sabor", $saboralimentoDieta);
              $inserir->bindParam(":marca", $marcaAlimentoDieta);
              $inserir->bindParam(":FK_id_animal", $id_animal);
              $inserir->execute();
              $tot_reg = $inserir->rowCount();
              if ($tot_reg > 0) {
                echo "<script>location.href='index.php?id=menu'</script>";
              }
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <!--FIM - Modal Adicionar Dieta -->


    <!--INICIO - Modal Adicionar Vacinas -->
    <div class="modal fade" id="AddVacinas" tabindex="-1" role="dialog" aria-labelledby="AddVacinasLabel" aria-hidden="true">
      <div class="modal-dialog " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="AddVacinasLabel">Adicionar vacina</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          
            
            <div class="modal-body">
              <form method="post">
                <input type="hidden" name="conf" value="1" />



                <div class="form-group">
                  <p class="text-muted mb-2">Nome da vacina</p>
                  <input type="text" class="form-control" name="nome" placeholder="Nome da vacina" required="required">
                </div>

                <div class="form-group">
                  <p class="text-muted mb-2">Data</p>
                  <input type="date" class="form-control" name="datarealizacao" required="required">
                </div>

                <div class="form-group">
                  <p class="text-muted mb-2">Retorno</p>
                  <input type="date" class="form-control" name="dataretorno" required="required">
                </div>




                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  <button type="submit" class="btn btn-success"name="AddVacina">Adicionar</button>
              </form>
            </div>
                
                
          <?php if (isset($_POST['AddVacina'])) {
            include_once("funcoes.php");
            $PDO = conectar();
            $id_animal = $_GET['id_animal'];
            $nomeVacina = strip_tags($_POST['nome']);
            $dataVacina = strip_tags($_POST['datarealizacao']);
            $retornoVacina = strip_tags($_POST['dataretorno']);
            $sql5 = "INSERT INTO vacinas (nome, datarealizacao, dataretorno, FK_id_animal) VALUES (:nome, :datarealizacao, :dataretorno, :FK_id_animal)";
            $inserir = $PDO->prepare($sql5);
            $inserir->bindParam(":nome", $nomeVacina);
            $inserir->bindParam(":datarealizacao", $dataVacina);
            $inserir->bindParam(":dataretorno", $retornoVacina);
            $inserir->bindParam(":FK_id_animal", $id_animal);
            $inserir->execute();
            $tot_reg = $inserir->rowCount();
            if ($tot_reg > 0) {
              echo "<script>location.href='index.php?id=menu'</script>";
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <!--FIM - Modal Adicionar Vacinas -->

  <!--INICIO - Modal Adicionar Exame -->
  <div class="modal fade" id="AddExame" tabindex="-1" role="dialog" aria-labelledby="AddExameLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="AddExameLabel">Adicionar exame</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    
          <div class="modal-body">
            <form method="post">
              <input type="hidden" name="conf" value="1" />



              <div class="form-group">
                <p class="text-muted mb-2">Nome do exame</p>
                <input type="text" class="form-control" name="nome" placeholder="Nome do exame realizado" required="required">
              </div>

              <div class="form-group">
                <p class="text-muted mb-2">Retorno</p>
                <div action="dogdados.php" method="POST" enctype="multipart/form-data"><input type="file" required name="img_exame"></div>
              </div>

              <div class="form-group">
                <p class="text-muted mb-2">Data</p>
                <input type="date" class="form-control" name="data" placeholder="data" required="required">
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-success" name="AddExame">Adicionar</button>
                  
            </form>
          </div>
        <?php if (isset($_POST['AddExame'])) {
          include_once("funcoes.php");
          $PDO = conectar();
          $id_animal = $_GET['id_animal'];
          $nomeExame = strip_tags($_POST['nome']);
          $imgExame = strip_tags($_POST['img_exame']);
          $dataExame = strip_tags($_POST['data']);
          $sql6 = "INSERT INTO exames (nome, img_exame, data, FK_id_animal) VALUES (:nome, :img_exame, :data, :FK_id_animal)";
          $inserir = $PDO->prepare($sql6);
          $inserir->bindParam(":nome", $nomeExame);
          $inserir->bindParam(":img_exame", $imgExame);
          $inserir->bindParam(":data", $dataExame);
          $inserir->bindParam(":FK_id_animal", $id_animal);
          $inserir->execute();
          $tot_reg = $inserir->rowCount();
          if ($tot_reg > 0) {
            echo "<script>location.href='index.php?id=menu'</script>";
          }
        }
        ?>
      </div>
    </div>
  </div>
  </div>
  <!--FIM - Modal Adicionar Exame -->
        
  <!--INICIO - Modal Editar Animal -->
  <div class="modal fade" id="MEditarAnimal" tabindex="-1" role="dialog" aria-labelledby="AddMEditarAnimal" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="AddMEditarAnimal">Editar Pet</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    
          <div class="modal-body">
            <form method="post">
              <input type="hidden" name="conf" value="1" />
                
                <?php $id_animal2 = $_GET['id_animal'];
                      
                ?>

              <!-- Nome -->
                      <div class="form-group">
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-paw"></i></span>
                          </div>

                          <input type="text" class="form-control" name="nome" placeholder="Nome" required="required">

                        </div>
                      </div>
                      <!-- Raça -->
                      <div class="form-group">
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-code-branch"></i></span>
                          </div>

                          <input type="text" class="form-control" name="raca" placeholder="Raça" required="required">

                        </div>
                      </div>

                      <!-- Data -->
                      <div class="form-group">
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                          </div>

                          <input type="date" class="form-control" name="data" placeholder="Data de Nascimento" required="required">

                        </div>
                      </div>

                      <!-- Peso -->
                      <div class="form-group">
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-dumbbell"></i></span>
                          </div>

                          <input type="text" class="form-control" name="peso" placeholder="Peso" required="required">

                        </div>
                      </div>



                      <!-- radiobuttons Fisico -->
                      <div class="mx-auto row my-4" style="width: 270px;">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Filhote" name="fisico" value="Filhote" class="custom-control-input">
                          <label class="custom-control-label" for="Filhote">Filhote</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Adulto" name="fisico" value="Adulto" class="custom-control-input">
                          <label class="custom-control-label" for="Adulto">Adulto</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Idoso" name="fisico" value="Idoso" class="custom-control-input">
                          <label class="custom-control-label" for="Idoso">Idoso</label>
                        </div>
                      </div>

                      <!-- radiobuttons Espécie -->
                      <div class="mx-auto row my-4" style="width: 300px;">


                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Gato" name="especie" value="Gato" class="custom-control-input">
                          <label class="custom-control-label" for="Gato"><i class="fas fa-cat"></i> Gato</label>
                        </div>


                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Cachorro" name="especie" value="Cachorro" class="custom-control-input">
                          <label class="custom-control-label" for="Cachorro"><i class="fas fa-dog"></i> Cachorro</label>
                        </div>


                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Outro" name="especie" value="Outro" class="custom-control-input">
                          <label class="custom-control-label" for="Outro">Outro</label>
                        </div>
                      </div>

                      <!-- radiobuttons Castrado -->
                      <h6 class="text-center">Castrado?</h6>
                      <div class="mx-auto row my-4" style="width: 140px;">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Sim" name="castrado" value="Sim" class="custom-control-input">
                          <label class="custom-control-label" for="Sim">Sim</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Nao" name="castrado" value="Não" class="custom-control-input">
                          <label class="custom-control-label" for="Nao">Não</label>
                        </div>
                      </div>

                      <!-- Imagem -->

                      <div class="d-flex flex-column justify-content-center align-items-center" style="font-size:14px">
                        <div action="cadastro_pets.php" method="POST" enctype="multipart/form-data"><input type="file" required name="img_pet"></div>

                        <li class="nav-item dropdown">
                          <a href='index.php?id=menu' class="nav-link">

                          </a>
                        </li>
                      </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-success" name="EditarAnimal">Adicionar</button>
                  
            </form>
          </div>
        <?php if (isset($_POST['EditarAnimal'])) {
          include_once("funcoes.php");
      $PDO = conectar();
      $sql1 = "SELECT * FROM animais WHERE FK_id_usuario = $idUsuario";
      $pesquisa= $PDO->prepare($sql);
	  $pesquisa->bindParam(':id_animal',$idAnimal);
      $pesquisa->execute();
      $resultado = $pesquisa->fetch(PDO::FETCH_ASSOC);
      $nomePet= $resultado['nome'];
      $racaPet = $resultado['raca'];
      $dataPet = $resultado['data'];
      $pesoPet = $resultado['peso'];
      $fisicoPet = $resultado['fisico'];
      $especiePet = $resultado['especie'];
      $castradoPet = $resultado['castrado'];
      $img_pet = $resultado['img_pet'];
      $sql = "UPDATE usuarios SET nome=:nome,raca=:raca,data=:data,peso=:peso,fisico:=:fisico, especie=especie, castrado:=castrado WHERE id_animal=:id_animal";
      $inserir = $PDO->prepare($sql);
      $inserir->bindParam(":nome", $nomePet);
      $inserir->bindParam(":raca", $racaPet);
      $inserir->bindParam(":data", $dataPet);
      $inserir->bindParam(":peso", $pesoPet);
      $inserir->bindParam(":fisico", $fisicoPet);
      $inserir->bindParam(":especie", $especiePet);
      $inserir->bindParam(":castrado", $castradoPet);
      $inserir->bindParam(":img_pet", $img_pet);
      $inserir->bindParam(":FK_id_usuario", $idUsuario);
      $inserir->execute();
      $tot_reg = $inserir->rowCount();
      if ($tot_reg > 0) {
        echo "<script>location.href='index.php?id=menu'</script>";
      }
    
    
    
    
    
    
        }
        ?>
      </div>
    </div>
  </div>
  </div>
  <!--FIM - Modal Editar Animal -->
 
  <!--INICIO - Dados Pet-->
  <section class="postagens">
    <div class="col-12 col-md-auto mb-4">

      <div class="row">
        <div class="col-sm-12">
          <div class="card shadow-lg">
            <div class="card-title mt-2">
              <?php

              $id_animal2 = $_GET['id_animal'];

              include_once("funcoes.php");
              $PDO = conectar();

              $sql2 = "SELECT * FROM animais WHERE id_animal = $id_animal2";
              $resultado2 = $PDO->prepare($sql2);
              $resultado2->execute();

              while ($dados = $resultado2->fetch(PDO::FETCH_ASSOC)) {

              ?>
                
                
                
              <h5 class="text-center">
                <strong style="color: #5193BD; font-size:30px;">
                    
                    <form method="POST">
                    <input class='btn btn-sm btn-danger' name="btnExcluirAnimal" type="submit" value="Excluir">
                    <?php
                      if (isset($_POST['btnExcluirAnimal'])) {
                          echo "<script>location.href='index.php?id=menu'</script>"; 
                          $excluirAnimal = "DELETE FROM animais WHERE id_animal = $id_animal2";
                          $exclusao = $PDO->prepare($excluirAnimal);
                          $exclusao->execute();}
                  ?><?php echo $dados['nome']; ?> 
                  
                        </form>
                <?php
               if (isset($id_animal)) {
                echo $id_animal;
                } else 
                {
                  echo "<button class='btn btn-sm btn-warning mb-1' data-toggle='modal' data-target='#MEditarAnimal'>Editar</button>";
                }?>
              
    
                    
                 </strong>
                </h5>

            </div>
            <!-- FIM CARD TITULO -->

            <img src="imagens/img_pets/<?php echo $dados['img_pet']; ?>" class="card-image-top" height="280" style="padding-right: 14px;
        padding-left: 14px;">

            <div class="overlay d-flex align-items-center justify-content-center">
            </div>

<div class="card-body">
<div class="container">
<p style="font-family: 'Lobster', cursive; font-size:20px;" class="text-center mb-0 mt-0">Raça: <h style="font-family: 'Comfortaa', cursive; font-size:15px;"><?php echo $dados['raca']; ?></p>
<p style="font-family: 'Lobster', cursive; font-size:20px;" class="text-center mb-0 mt-0">Peso: <h style="font-family: 'Comfortaa', cursive; font-size:15px;"><?php echo $dados['peso']; ?></p>
<p style="font-family: 'Lobster', cursive; font-size:20px;" class="text-center mb-0 mt-0">Físico: <h style="font-family: 'Comfortaa', cursive; font-size:15px;"><?php echo $dados['fisico']; ?></p>
<p style="font-family: 'Lobster', cursive; font-size:20px;" class="text-center mb-0 mt-0">Espécie: <h style="font-family: 'Comfortaa', cursive; font-size:15px;">
    <?php
    if ($dados['especie'] == "Cachorro") {
    ?><?php echo $dados['especie'];?> <i class="fas fa-dog"></i>
    <?php
                                                                }
    if ($dados['especie'] == "Gato") {
    ?><?php echo $dados['especie'];?> <i class="fas fa-cat"></i><?php
    } ?>
 </p>
<p style="font-family: 'Lobster', cursive; font-size:20px;" class="text-center mb-0 mt-0">Castrado: <h style="font-family: 'Comfortaa', cursive; font-size:15px;"><?php echo $dados['castrado']; ?></p>
<p style="font-family: 'Lobster', cursive; font-size:20px;" class="text-center mb-0 mt-0">Data de Nascimento: <h style="font-family: 'Comfortaa', cursive; font-size:15px;"><?php echo $dados['data']; ?></p>
    </div>

</div>

              <?php } ?>




          </div>
        </div>
      </div>
      <!--FIM - Dados Pet-->


      <!--INICIO - Dieta -->
      <div class="row mt-3">
        <div class="col-md-12">
          <div class="card shadow-lg">
            <div class="card-title mt-2">
              <h5 class="text-center"><strong style="font-size:25px">Dieta <?php
               if (isset($tipoalimentoDieta)) {
                echo $tipoalimentoDieta;
                } else 
                {
                  echo "<button class='btn btn-sm btn-success' data-toggle='modal' data-target='#AddDieta'>Add</button>";
                }?>
                </strong></h5>

            </div>


            <div class="overlay d-flex align-items-center justify-content-center">


              <div class="card-body">
                <div class="table-responsive-lg">
                  <table class="table table-striped ">
                    <thead>
                      <tr>
                        <th scope="col">Alimento</th>
                        <th scope="col">Sabor</th>
                        <th scope="col">Marca</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <?php
    $sql5 = "SELECT * FROM dieta WHERE FK_id_animal = $id_animal2";
    $resultado5 = $PDO->prepare($sql5);
    $resultado5->execute();

    while ($dados4 = $resultado5->fetch(PDO::FETCH_ASSOC)) {
    $id_dieta = $dados4['id_dieta'];
    
    ?>
                    <tbody>
                      <tr>

                        <th scope="row"><?php echo $dados4['alimento']; ?></th>
                        <td><?php echo $dados4['sabor']; ?></td>
                        <td><?php echo $dados4['marca']; ?></td>



                        <td class="align-items-center justify-content-center">
                          <?php
                          if (isset($tipoalimentoDieta)) {
                            echo $tipoalimentoDieta;
                          } else {
                            echo "<button class='btn btn-sm btn-warning mb-1' data-toggle='modal' data-target='#EditarUsuario'>Editar</button>";
                          }
                          ?>
                          
                            
                        <form method="POST">
                    <input class='btn btn-sm btn-danger' name="btnExcluirDieta" type="submit" value="Excluir">
                    <?php
                      if (isset($_POST['btnExcluirDieta'])) {
                          echo "<script>location.href='index.php?id=menu'</script>"; 
                          $excluirDieta = "DELETE FROM Dieta WHERE id_dieta = $id_dieta";
                          $exclusao2 = $PDO->prepare($excluirDieta);
                          $exclusao2->execute();}
                          ?>
                        </form>
                            

                        </td>
                      </tr>
                      <tr>

                      </tr>


                    </tbody>
                        <?php }?>

                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--FIM - Dieta -->
    </div>
    <!--INICIO - Exame -->

    <div class="col-12 col-md-auto">
      <div class="card shadow-lg">
        <div class="card-title mt-2">
          <h5 class="text-center"><strong style="font-size:25px">Exame <?php  if (isset($tipoalimentoDieta)) { echo $tipoalimentoDieta;} else {
          echo "<button class='btn btn-sm btn-success' data-toggle='modal' data-target='#AddExame'>Add</button>"; } ?>
            </strong></h5>
        </div>
        <div class="overlay d-flex align-items-center justify-content-center">



          <div class="card-body">

            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Nome</th>
                  <th scope="col">Imagem</th>
                  <th scope="col">Data</th>
                  <th></th>


                </tr>
              </thead>
    <?php
    $sql3 = "SELECT * FROM exames WHERE FK_id_animal = $id_animal2";
    $resultado3 = $PDO->prepare($sql3);
    $resultado3->execute();
    
    while ($dados2 = $resultado3->fetch(PDO::FETCH_ASSOC)) {
    
    $id_exame = $dados2['id_exame']; 
    
    
    ?>
              <tbody>
                <tr>
                  <th scope="row"><?php echo $dados2['nome']; ?></th>
                  <td><?php echo $dados2['img_exame']; ?></td>
                  <!--botão com texto sublinhado - Abrir imagem em um modal-->
                  <td><?php echo $dados2['data']; ?></td>
                  <td><?php echo $id_exame ?></td>
    
                  <td class="align-items-center justify-content-center">
                    <?php
                    if (isset($tipoalimentoDieta)) {
                      echo $tipoalimentoDieta;
                    } else {
                      echo "<button class='btn btn-sm btn-warning mb-1' data-toggle='modal' data-target='#EditarUsuario'>Editar</button>";
                    }
                    ?>
                    
                      <form method="POST">
                    <input class='btn btn-sm btn-danger' name="btnExcluirExame" type="submit" value="Excluir">
                    <?php
                      if (isset($_POST['btnExcluirExame'])) {
                          echo "<script>location.href='index.php?id=menu'</script>"; 
                          $excluirExame = "DELETE FROM Exames WHERE id_exame = '$id_exame'";
                          $exclusao3 = $PDO->prepare($excluirExame);
                          $exclusao3->execute();}
                          ?>
                        </form>
                      
                      
                  </td>
                </tr>
              </tbody>

              <?php }?>
              
            </table>

          </div>

        </div>
      </div>
      <!--FIM - Exame -->

      <!--INICIO - Vacina -->
      <div class="card shadow-lg mt-4">
        <div class="card-title mt-2">
          <h5 class="text-center"><strong style="font-size:25px">Vacinas <?php
                                                                          if (isset($tipoalimentoDieta)) {
                                                                            echo $tipoalimentoDieta;
                                                                          } else {
                                                                            echo "<button class='btn btn-sm btn-success' data-toggle='modal' data-target='#AddVacinas'>Add</button>";
                                                                          }
                                                                          ?>
            </strong></h5>
        </div>
        <div class="overlay d-flex align-items-center justify-content-center">



          <div class="card-body">
            <table class="table table-striped ">
              <thead>
                <tr>
                  <th scope="col">Vacinas</th>
                  <th scope="col">Data</th>
                  <th scope="col">Retorno</th>
                  <th></th>
                </tr>
              </thead>
              <?php
    $sql4 = "SELECT * FROM vacinas WHERE FK_id_animal = $id_animal2";
    $resultado4 = $PDO->prepare($sql4);
    $resultado4->execute();

    while ($dados3 = $resultado4->fetch(PDO::FETCH_ASSOC)) {
     $id_vacina = $dados3['id_vacina'];
    ?>
              <tbody>
                <tr>
                  <th scope="row"><?php echo $dados3['nome']; ?></th>
                  <td><?php echo $dados3['datarealizacao']; ?></td>
                  <td><?php echo $dados3['dataretorno']; ?></td>
                  <td class="align-items-center justify-content-center">
                    <?php
                    if (isset($tipoalimentoDieta)) {
                      echo $tipoalimentoDieta;
                    } else {
                      echo "<button class='btn btn-sm btn-warning mb-1' data-toggle='modal' data-target='#EditarExame'>Editar</button>";
                    }
                    ?>
                    
                      
                      <form method="POST">
                    <input class='btn btn-sm btn-danger' name="btnExcluirVacina" type="submit" value="Excluir">
                    <?php
                      if (isset($_POST['btnExcluirVacina'])) {
                          echo "<script>location.href='index.php?id=menu'</script>"; 
                          $excluirVacina = "DELETE FROM Vacinas WHERE id_vacina == $id_vacina";
                          $exclusao4 = $PDO->prepare($excluirVacina);
                          $exclusao4->execute();}
                          ?>
                        </form>
                      

                  </td>
                </tr>
                <tr>
                  <th scope="row"></th>
                  <td></td>
                </tr>
              </tbody>
              <?php }?>
            </table>
          </div>




        </div>

      </div>
        
        
        
         <!--FIM - Vacina -->
        
         <!--INICIO - Relatório -->

      <div class="card shadow-lg mt-4 mb-4 align-items-center justify-content-center">
        <div class="card-title mt-4 mb-4 ">

            
            <div class="btn-wrapper">
			<a href="pdf.php?id=id_animal=<?php echo $id_animal2;?>" class='btn btn-lg btn-info'>

			<span>Gerar Relatório</span>
			</a>
			</div>
            
            
        </div>
      </div>
        
        
        
        
      <!--FIM - Relatório -->
    </div>
  </section>
  </div>
  </div>
  <!-- Bootstrap core JavaScript -->
  <script src="assets/jquery/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>