<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Título -->
  <title>Attention Pets</title>
  <link rel="icon" type="imagem/png" href="imagens/logos/icon.png" />

  <!-- Estilos -->
	<link href="estilo.css" rel="stylesheet">
   

<style>
body {
      background-color: #EDEDED;
      font-family: "Lato", sans-serif;
    }

    * {
      /*       border: 1px red solid; */

    }

    body {
      overflow-x: hidden;
    }

    .list-group-item {
      font-family: 'Comfortaa', cursive;
      font-size: 13px;
      text-indent: 12px;
      color: black;
    }

    #sidebar-wrapper {
      margin-left: -15rem;
      -webkit-transition: margin .25s ease-out;
      -moz-transition: margin .25s ease-out;
      -o-transition: margin .25s ease-out;
      transition: margin .25s ease-out;
    }

    #sidebar-wrapper a {
      border-radius: 2px;
      text-decoration: none;
      font-size: 13px;
      display: block;
      transition: 0.3s;
    }

    #sidebar-wrapper .sidebar-heading {
      padding: 0.875rem 1.25rem;
      font-size: 1.2rem;

    }

    #sidebar-wrapper .list-group {
      width: 15rem;
    }

    #page-content-wrapper {
      min-width: 100vw;
    }

    #wrapper.toggled #sidebar-wrapper {
      margin-left: 0;
    }
      
@media (min-width: 768px) {
  #sidebar-wrapper {
    margin-left: 0;
  }

  #page-content-wrapper {
    min-width: 0;
    width: 100%;
  }

  #wrapper.toggled #sidebar-wrapper {
    margin-left: -15rem;
  }
}
.bg-light {
  background-color: #f8f9fa !important;
}

a.bg-light:hover, a.bg-light:focus,
button.bg-light:hover,
button.bg-light:focus {
  background-color: #dae0e5 !important;
}
.card > .list-group:first-child .list-group-item:first-child {
  border-top-left-radius: 0.25rem;
  border-top-right-radius: 0.25rem;
}

.card > .list-group:last-child .list-group-item:last-child {
  border-bottom-right-radius: 0.25rem;
  border-bottom-left-radius: 0.25rem;
}
.list-group-flush .list-group-item {
  border-right: 0;
  border-left: 0;
  border-radius: 0;
}

.list-group-flush:first-child .list-group-item:first-child {
  border-top: 0;
}

.list-group-flush:last-child .list-group-item:last-child {
  border-bottom: 0;
}    
   .list-group-item-action {
  width: 100%;
  color: #495057;
  text-align: inherit;
}

.list-group-item-action:hover, .list-group-item-action:focus {
  color: #495057;
  text-decoration: none;
  background-color: #f8f9fa;
}

.list-group-item-action:active {
  color: #212529;
  background-color: #e9ecef;
}

.list-group-item {
  position: relative;
  display: block;
  padding: 0.75rem 1.25rem;
  margin-bottom: -1px;
  background-color: #fff;
  border: 1px solid rgba(0, 0, 0, 0.125);
}

.list-group-item:first-child {
  border-top-left-radius: 0.25rem;
  border-top-right-radius: 0.25rem;
}

.list-group-item:last-child {
  margin-bottom: 0;
  border-bottom-right-radius: 0.25rem;
  border-bottom-left-radius: 0.25rem;
}

.list-group-item:hover, .list-group-item:focus {
  z-index: 1;
  text-decoration: none;
}

.list-group-item.disabled, .list-group-item:disabled {
  color: #6c757d;
  background-color: #fff;
}

.list-group-item.active {
  z-index: 2;
  color: #fff;
  background-color: #007bff;
  border-color: #007bff;
}   
      .list-group-item-action {
  width: 100%;
  color: #495057;
  text-align: inherit;
}

.list-group-item-action:hover, .list-group-item-action:focus {
  color: #495057;
  text-decoration: none;
  background-color: #f8f9fa;
}

.list-group-item-action:active {
  color: #212529;
  background-color: #e9ecef;
}
      .card-body {
          width: 500px;
      }
  </style>
</head>

<body id="inicio">
  

 <div class="d-flex" id="wrapper">

   <!-- Sidebar -->
    <div class="bg-light" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-bottom: 17px;"><img src="imagens/logos/Logo_Nav_Icon.png" width="200" class="img-fluid"></div>
      <div class="list-group list-group-flush">
        
        <a href="index.php?id=menu" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/house.svg" class="mb-2 mt-2" width="30" > Início</a>
          
        <a href="index.php?id=cadastro_pets" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/dog.svg" width="30" class="mb-2 mt-2"> Cadastrar Pet</a>
          
        <a href="index.php?id=menu#faleconosco" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/network.svg" class="mb-2 mt-2" width="30"> Fale Conosco</a>
          
        <a href="index.php?id=menu#mapa" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/maps-and-flags.svg" class="mb-2 mt-2" width="30"> Mapa</a>
        
          <?php
        @session_start();
        if (isset($_SESSION['logado'])) {
          ?>
          <!--INICIO - Sair-->
          <a href='index.php?id=logout' class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/logout.svg" class="mb-2 mt-2" width="30"> Sair</a>
        <!--FIM - Sair-->
        <?php
        }
        ?>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

     <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <img src="imagens/icons/menu-button.svg" class="mb-2 mt-2" width="30" id="menu-toggle" style="cursor:pointer;"> 
      </nav>      

  <!--FIM - Menu-->
<?php
@session_start();
if(isset($_SESSION['logado'])){
$idUsuario = $_SESSION['idUsuario'];
$logado=$_SESSION['logado'];
}else {
   $logado=0;
      echo "<script>location.href='index.php'</script>";
}
?>

        <!--INICIO - Cadastro-->
    <?php
    if (!isset($_POST['conf'])) {
      ?>
        
        
<div style="background-image:url(imagens/teste3.png);">
    
      <div class="cad-form">
        <form action="index.php?id=cadastro_pets" method="post">
          <input type="hidden" name="conf" value="1" />
<div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-5 mt-5">
            <div class="card bg-secondary shadow border-0"> 
              <div style="padding-right: 20px;padding-left: 20px;padding-bottom: 30px;padding-top: 40px;">
                <div class="text-center text-muted mb-4">
                  <h5 style="font-size:30px;">Cadastro de pets  <img src="imagens/icons/pet.svg" width="30"></h5>
                </div>
                    <div class="text-center text-muted mb-4">
                      <small>Preencha os campos abaixo!</small>
                    </div>
                
                    <form role="form">

                      <!-- Nome -->
                      <div class="form-group">
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-paw"></i></span>
                          </div>

                          <input type="text" class="form-control" name="nome" placeholder="Nome" required="required">

                        </div>
                      </div>
                      <!-- Raça -->
                      <div class="form-group">
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-code-branch"></i></span>
                          </div>

                          <input type="text" class="form-control" name="raca" placeholder="Raça" required="required">

                        </div>
                      </div>

                      <!-- Data -->
                      <div class="form-group">
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                          </div>

                          <input type="date" class="form-control" name="data" placeholder="Data de Nascimento" required="required">

                        </div>
                      </div>

                      <!-- Peso -->
                      <div class="form-group">
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-dumbbell"></i></span>
                          </div>

                          <input type="text" class="form-control" name="peso" placeholder="Peso" required="required">

                        </div>
                      </div>



                      <!-- radiobuttons Fisico -->
                      <div class="mx-auto row my-4" style="width: 270px;">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Filhote" name="fisico" value="Filhote" class="custom-control-input">
                          <label class="custom-control-label" for="Filhote">Filhote</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Adulto" name="fisico" value="Adulto" class="custom-control-input">
                          <label class="custom-control-label" for="Adulto">Adulto</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Idoso" name="fisico" value="Idoso" class="custom-control-input">
                          <label class="custom-control-label" for="Idoso">Idoso</label>
                        </div>
                      </div>

                      <!-- radiobuttons Espécie -->
                      <div class="mx-auto row my-4" style="width: 300px;">


                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Gato" name="especie" value="Gato" class="custom-control-input">
                          <label class="custom-control-label" for="Gato"><i class="fas fa-cat"></i> Gato</label>
                        </div>


                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Cachorro" name="especie" value="Cachorro" class="custom-control-input">
                          <label class="custom-control-label" for="Cachorro"><i class="fas fa-dog"></i> Cachorro</label>
                        </div>


                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Outro" name="especie" value="Outro" class="custom-control-input">
                          <label class="custom-control-label" for="Outro">Outro</label>
                        </div>
                      </div>

                      <!-- radiobuttons Castrado -->
                      <h6 class="text-center">Castrado?</h6>
                      <div class="mx-auto row my-4" style="width: 140px;">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Sim" name="castrado" value="Sim" class="custom-control-input">
                          <label class="custom-control-label" for="Sim">Sim</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="Nao" name="castrado" value="Não" class="custom-control-input">
                          <label class="custom-control-label" for="Nao">Não</label>
                        </div>
                      </div>

                      <!-- Imagem -->

                      <div class="d-flex flex-column justify-content-center align-items-center" style="font-size:14px">
                        <div action="cadastro_pets.php" method="POST" enctype="multipart/form-data"><input type="file" required name="img_pet"></div>

                        <div class="mx-auto mt-3" style="width: 200px;">
                          <button type="submit" class="btn btn-info btn-block">CADASTRAR</button>
                        </div>

                        <li class="nav-item dropdown">
                          <a href='index.php?id=menu' class="nav-link">

                            <span class="nav justify-content-center">Cancelar</span>
                          </a>
                        </li>
                      </div>

                    </form>
                  </div>
                </div>
            </div>
            </div>
          </div>
        </form>
      </div>
    <br><br>
    </div></div></div>
    <?php } else {
      include_once("funcoes.php");
      $PDO = conectar();
      $sql1 = "SELECT * FROM animais WHERE FK_id_usuario = $idUsuario";
      $nomeLogin = strip_tags($_POST['nome']);
      $racaLogin = strip_tags($_POST['raca']);
      $dataLogin = strip_tags($_POST['data']);
      $pesoLogin = strip_tags($_POST['peso']);
      $fisicoLogin = strip_tags($_POST['fisico']);
      $especieLogin = strip_tags($_POST['especie']);
      $castradoLogin = strip_tags($_POST['castrado']);
      $img_pet = strip_tags($_POST['img_pet']);
      $sql = "INSERT INTO animais (nome, raca, data, peso, fisico, especie, castrado,img_pet, FK_id_usuario) VALUES (:nome, :raca, :data, :peso, :fisico, :especie, :castrado, :img_pet, :FK_id_usuario)";
      $inserir = $PDO->prepare($sql);
      $inserir->bindParam(":nome", $nomeLogin);
      $inserir->bindParam(":raca", $racaLogin);
      $inserir->bindParam(":data", $dataLogin);
      $inserir->bindParam(":peso", $pesoLogin);
      $inserir->bindParam(":fisico", $fisicoLogin);
      $inserir->bindParam(":especie", $especieLogin);
      $inserir->bindParam(":castrado", $castradoLogin);
      $inserir->bindParam(":img_pet", $img_pet);
      $inserir->bindParam(":FK_id_usuario", $idUsuario);
      $inserir->execute();
      $tot_reg = $inserir->rowCount();
      if ($tot_reg > 0) {
        echo "<script>location.href='index.php?id=menu'</script>";
      }
    }
    ?>
<!--FIM - Cadastro-->  
        
<!-- Bootstrap core JavaScript -->
  <script src="assets/jquery/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
    
</body>
</html>