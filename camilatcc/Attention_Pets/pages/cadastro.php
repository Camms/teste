<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Título -->
	<title>Attention Pets</title>
	<link rel="icon" type="imagem/png" href="imagens/logos/pet.png" />

	<!-- Estilos -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="estilo.css" rel="stylesheet">

</head>
<body id="Topo">
<!--INICIO - Menu-->
		<header class="header-global">
			<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
				<div class="container">

					<!--Logo-->
					<a class="navbar js-scroll-trigger" href="index.php">
						<img src="imagens/icons/pet.svg" width="40"> <img src="imagens/logos/Logo_Nav.png" width="200px">
					</a>

					<!-- Botão Menu -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<!-- Divisão/Titulo do Botão -->
					<div class="navbar-collapse collapse" id="navbar_global">
						<div class="navbar-collapse-header">
							<div class="row">
								<div class="col-10 collapse-brand">
									<a href="./index.html">
										<img src="imagens/icons/pet.svg" width="35"><img src="imagens/logos/Logo_Nav_Resp.png" width="200">
									</a>
								</div>

								<!-- Botão Fechar Menu -->
								<div class="col-2 collapse-close">
									<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
										<span></span>
										<span></span>
									</button>
								</div>

							</div>
						</div>

						<!-- Botão Menu Dentro -->
						<ul class="navbar-nav navbar-nav-hover align-items-lg-center">

							<!-- Fale Conosco -->
							<li class="nav-item dropdown">
								<a href="#contact" class="nav-link js-scroll-trigger" role="button">
									<span class="nav-link-inner--text" >Fale Conosco</span>
								</a>
								<div class="dropdown-menu dropdown-menu-lg">
									<div class="dropdown-menu-inner">

										<a href="#contact" class="media d-flex  align-items-center js-scroll-trigger">
											<div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
												<i class="fas fa-comments"></i>
											</div>
											<div class="media-body ml-3">
												<h5 class="text-warning mb-md-1">Fale Conosco</h5>
												<p class="description d-none d-md-inline-block mb-0" style="font-family: 'Comfortaa', cursive; font-size:13px;">Deseja dar alguma sugestão para melhorias do site? Clique aqui e fale conosco!</p>
											</div>
										</a>

									</div>
								</div>
							</li>

							<!-- Outros -->
							<li class="nav-item dropdown">
								<a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
									<span class="nav-link-inner--text">Outros</span>
								</a>
								<div class="dropdown-menu dropdown-menu-xl">
									<div class="dropdown-menu-inner">

										<a href="#entrevistas" class="media d-flex align-items-center js-scroll-trigger">
											<div class="icon icon-shape bg-gradient-primary rounded-circle text-white">
												<i class="fas fa-clipboard-check"></i>
											</div>
											<div class="media-body ml-3">
												<h5 class="text-primary mb-md-1">Entrevistas</h5>
												<p class="description d-none d-md-inline-block mb-0" style="font-family: 'Comfortaa', cursive; font-size:13px;">Algumas pessoas foram selecionadas para testar as funcionalidades do site. Clique e veja o que elas tem a dizer!</p>
											</div>
										</a>

										<a href="#" class="media d-flex align-items-center js-scroll-trigger">
											<div class="icon icon-shape bg-gradient-success rounded-circle text-white">
												<i class="fas fa-stream"></i>
											</div>
											<div class="media-body ml-3">
                                                <h5 class="text-success mb-md-1">Desenvolvedora<h5 class="text-warning mb-md-1"></h5>
												<p class="description d-none d-md-inline-block mb-0" style="font-family: 'Comfortaa', cursive; font-size:13px;">Deseja entrar em contato com a desenvolvedora para assuntos profissionais? Clique Aqui!</p>
											</div>
										</a>

									</div>
								</div>
							</li>
						</ul>
						<ul class="navbar-nav align-items-lg-center ml-lg-auto"> 
              <?php
            @session_start();
            if(isset($_SESSION['logado'])){
              ?>
              <!--INICIO - Sair-->
              <li class="nav-item dropdown">
                <a href='index.php?id=logout' class="nav-link">
                 <span class="nav-link-inner--text">Sair</span>
                </a>
              </li>
              <!--FIM - Sair-->
              <?php
            }else{
              ?>
              <!--INICIO - Entrar-->
			  <li class="nav-item dropdown">
              <a href="index.php?id=login"  class="nav-link">
				<div class="btn-entrar">
                <span class="nav-link-inner--text"><b>Entrar</b></span>
                </div>
              </a>
              </li>
              <!--FIM - Entrar-->
                            
                            
                        
                            
              <?php
            }
          ?>
              </ul>  

					</div>
				</div>
			</nav>
		</header>
		<!--FIM - Menu-->
<?php
@session_start();
if(isset($_SESSION['logado'])){
$logado=$_SESSION['logado'];
}else {
   $logado=0;
}
if($logado==1){
	  echo "<script>location.href='index.php?id=menu'</script>";
}else {
?>
  <?php } ?>
<div class="fundo" style="background-image: url('imagens/carousel/carousel1.png')">
<br><br><br>
<!--INICIO - Cadastro-->
<br><br><br>
<?php
  if(!isset($_POST['conf'])){
 ?>
<div class="login-form">
<form action="index.php?id=cadastro" method="post">
<input type="hidden" name="conf" value="1" />
      <div class="container pt-lg-sm">
        <div class="row justify-content-center">
          <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0"> 
              <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                  <h5><b>Cadastro</b>, é rápido e fácil  <img src="imagens/icons/pet.svg" width="30"></h5>
                </div>
                <form role="form">
                    
                     <!-- Nome -->
                  <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-pencil-alt"></i></span>
                      </div>
                        
                      <input type="text" class="form-control" name="nome" placeholder="Nome Completo" required="required" style="text-indent: 5px;">
                        
                    </div>
                  </div>
                    <!-- Usuário -->
                    <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                      </div>
                        
                      <input type="text" class="form-control" name="usuario" placeholder="Usuario" required="required" style="text-indent: 5px;">
                        
                    </div>
                  </div>
                    
                    <!-- Email --> 
                  <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                      </div>
                        
                      <input type="email" class="form-control" name="email" placeholder="E-mail" required="required" style="text-indent: 5px;">
                        
                    </div>
                  </div>
                    
                     <!-- Senha -->
                  <div class="form-group">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                      </div>
                        
                      <input type="password" class="form-control" name="senha" placeholder="Senha" required="required" style="text-indent: 5px;">				   
                    </div>
                  </div>
                    
                    <div class="form-group">
                  <div class="row my-4">
                    <div class="col-12">
                      <div class="custom-control custom-control-alternative custom-checkbox">
                        <input class="custom-control-input" id="customCheckRegister" type="checkbox" required="required">  
                        <label class="custom-control-label" for="customCheckRegister">
                          <span style="font-family: 'Comfortaa', cursive; font-size:12px">Eu concordo com a
                            <a href="#" style="font-size:12px" >Política de Privacidade</a>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                   </div>
                        
                  <div class="form-group">
                   <button type="submit" class="btn btn-info btn-block">CADASTRAR</button>
                  </div>
                </form>
              </div>
            </div>
              
              <div class="row mt-3">
              <div class="col-12 text-right">
                <a href="index.php?id=login" class="text-light">
                  <small style="font-family: 'Comfortaa', cursive; font-size:12px">Já possui uma conta?</small>
                </a>
                  
              </div>
            </div>
        </div>
    </div>  
</div>
</form>
</div>
<?php } else
{
	include_once("funcoes.php");
	$PDO = conectar();
    $nomeLogin = strip_tags($_POST['nome']);
	$usuarioLogin = strip_tags($_POST['usuario']);
	$emailLogin = strip_tags($_POST['email']);
    $senhaLogin = strip_tags($_POST['senha']);
	$sql="INSERT INTO usuarios (nome, usuario, email, senha) VALUES (:nome, :usuario, :email, :senha)";
	$inserir= $PDO->prepare($sql);
    $inserir->bindParam(":nome",$nomeLogin);
	$inserir->bindParam(":usuario",$usuarioLogin);
	$inserir->bindParam(":email",$emailLogin);
    $inserir->bindParam(":senha",$senhaLogin);  
	$inserir->execute();
	$tot_reg = $inserir->rowCount();
	if($tot_reg>0)
	{
  	echo "<script>location.href='index.php'</script>";
	}
}
 ?>      
</div>
  <!--FIM - Cadastro-->
</body>
</html>