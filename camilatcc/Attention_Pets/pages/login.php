<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Título -->
	<title>Attention Pets</title>
	<link rel="icon" type="imagem/png" href="imagens/logos/icon.png"/>
    
	<!-- Estilos -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="estilo.css" rel="stylesheet">
    
    <style>
        .modal-footer-edt{
            display: flex;
            justify-content: flex-end;
        }</style>
    
    </head>
<body id="Topo">
<!--INICIO - Menu-->
		<header class="header-global">
			<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
				<div class="container">

					<!--Logo-->
					<a class="navbar js-scroll-trigger" href="index.php">
						<img src="imagens/icons/pet.svg" width="40"> <img src="imagens/logos/Logo_Nav.png" width="200px">
					</a>

					<!-- Botão Menu -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<!-- Divisão/Titulo do Botão -->
					<div class="navbar-collapse collapse" id="navbar_global">
						<div class="navbar-collapse-header">
							<div class="row">
								<div class="col-10 collapse-brand">
									<a href="./index.html">
										<img src="imagens/icons/pet.svg" width="35"><img src="imagens/logos/Logo_Nav_Resp.png" width="200">
									</a>
								</div>

								<!-- Botão Fechar Menu -->
								<div class="col-2 collapse-close">
									<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
										<span></span>
										<span></span>
									</button>
								</div>

							</div>
						</div>

						<!-- Botão Menu Dentro -->
						<ul class="navbar-nav navbar-nav-hover align-items-lg-center">

							<!-- Fale Conosco -->
							<li class="nav-item dropdown">
								<a href="index.php?#contact" class="nav-link js-scroll-trigger" role="button">
									<span class="nav-link-inner--text" >Fale Conosco</span>
								</a>
								<div class="dropdown-menu dropdown-menu-lg">
									<div class="dropdown-menu-inner">

										<a href="index.php?#contact" class="media d-flex  align-items-center js-scroll-trigger">
											<div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
												<i class="fas fa-comments"></i>
											</div>
											<div class="media-body ml-3">
												<h5 class="text-warning mb-md-1">Fale Conosco</h5>
												<p class="description d-none d-md-inline-block mb-0" style="font-family: 'Comfortaa', cursive; font-size:13px;">Deseja dar alguma sugestão para melhorias do site? Clique aqui e fale conosco!</p>
											</div>
										</a>

									</div>
								</div>
							</li>

							<!-- Outros -->
							<li class="nav-item dropdown">
								<a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
									<span class="nav-link-inner--text">Outros</span>
								</a>
								<div class="dropdown-menu dropdown-menu-xl">
									<div class="dropdown-menu-inner">

										<a href="index.php?#entrevistas" class="media d-flex align-items-center js-scroll-trigger">
											<div class="icon icon-shape bg-gradient-primary rounded-circle text-white">
												<i class="fas fa-clipboard-check"></i>
											</div>
											<div class="media-body ml-3">
												<h5 class="text-primary mb-md-1">Entrevistas</h5>
												<p class="description d-none d-md-inline-block mb-0" style="font-family: 'Comfortaa', cursive; font-size:13px;">Algumas pessoas foram selecionadas para testar as funcionalidades do site. Clique e veja o que elas tem a dizer!</p>
											</div>
										</a>

										<a href="#" class="media d-flex align-items-center js-scroll-trigger">
											<div class="icon icon-shape bg-gradient-success rounded-circle text-white">
												<i class="fas fa-stream"></i>
											</div>
											<div class="media-body ml-3">
                                                <h5 class="text-success mb-md-1">Desenvolvedora<h5 class="text-warning mb-md-1"></h5>
												<p class="description d-none d-md-inline-block mb-0" style="font-family: 'Comfortaa', cursive; font-size:13px;">Deseja entrar em contato com a desenvolvedora para assuntos profissionais? Clique Aqui!</p>
											</div>
										</a>

									</div>
								</div>
							</li>
						</ul>
						<ul class="navbar-nav align-items-lg-center ml-lg-auto"> 
              <?php
            @session_start();
            if(isset($_SESSION['logado'])){
              ?>
              <!--INICIO - Sair-->
              <li class="nav-item dropdown">
                <a href='index.php?id=logout' class="nav-link">
                 <span class="nav-link-inner--text">Sair</span>
                </a>
              </li>
              <!--FIM - Sair-->
              <?php
            }else{
              ?>
              <!--INICIO - Entrar-->
			  <li class="nav-item dropdown">
              <a href="index.php?id=login"  class="nav-link">
				<div class="btn-entrar">
                <span class="nav-link-inner--text"><b>Entrar</b></span>
                </div>
              </a>
              </li>
              <!--FIM - Entrar-->
              <?php
            }
          ?>
              </ul>  

					</div>
				</div>
			</nav>
		</header>
		<!--FIM - Menu-->
<?php
@session_start();
if(isset($_SESSION['logado'])){
$logado=$_SESSION['logado'];
}else {
   $logado=0;
}
if($logado==1){
	  echo "<script>location.href='index.php?id=menu'</script>";
}else {
?>
  <?php } ?>
<div class="fundo" style="background-image: url('imagens/carousel/carousel1.png')">
<br><br><br>
<!--INICIO - Login-->
    <br><br><br>

    
    
    <!-- Modal -->
<div class="modal fade" id="EsqueceuSenha" tabindex="-1" role="dialog" aria-labelledby="EsqueceuSenhaLabel" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="EsqueceuSenhaLabel" style="font-size:25px;">Recuperar Senha</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="">
          <p class="section-subheading text-muted" style="margin-bottom: 20px; font-style: italic;margin-right:10px; font-size:15px">Para recuperar sua senha é simples! Informe seu <b>e-mail</b> no campo abaixo e receba ela por email.</p>
          
          <div class="form-group">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                      </div>
                    
                      <input type="email" class="form-control" name="email" placeholder="E-mail" required="required" style="font-size:12px; text-indent: 5px;">
                        
                    </div>
            </div>
        </form>
      
        <div class="modal-footer-edt">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-info">Recuperar</button>
      </div>
      </div>
      
    </div>
  </div>
</div>
    
    
	<div class="login-page">
      <div class="container pt-lg-md">
        <div class="row justify-content-center">
          <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0">
              <div class="card-body px-lg-5 py-lg-5">
                  <div class="text-center text-muted mb-4">
                  <h5><b>Boas-vindas</b> de volta! <img src="imagens/icons/pet.svg" width="30"></h5>
                  <small class="section-subheading text-muted" style="margin-bottom: 20px; font-style: italic;margin-right:10px;">Informe os dados da sua conta abaixo</small>
                </div>
                <div class="form">
				<form class="login-form" method="post" action="pages/controle.php">
                    
                    <!--Usuário-->
                  <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                      </div>
                      <input type="text" class="form-control" name="usuario" placeholder="Usuario" required="required" style="text-indent: 5px;">
                    </div>
                  </div>
                    
                   <!--Senha--> 
                  <div class="form-group">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                      </div>
                      <input type="password" class="form-control"name="senha" placeholder="Senha" required="required" style="text-indent: 5px;">
                    </div>
                  </div>
				  

                  <div class="custom-control custom-control-alternative custom-checkbox">
                    <input class="custom-control-input" id=" customCheckLogin" name="customCheckLogin" type="checkbox">
                    <label class="custom-control-label" for=" customCheckLogin">
                      <span style="font-family: 'Comfortaa', cursive; font-size:12px">Lembrar-me</span>
                    </label>
                  </div>
                    
                <?php
if(isset($_POST["customCheckLogin"])){
$senha=$_POST["senha"];
$tempo_expiracao= 3600; //uma hora
 setcookie("lembrar", $senha, $tempo_expiracao);}
?>
                    
                  <div class="text-center">
                   <button type="submit" class="btn btn-info btn-block my-4">ENTRAR</button>
                  </div>
                    
                 </form>   
                </div>
              </div>
            </div>
              
            <div class="row mt-3">
              <div class="col-6">
                <a class="text-light">
                  <small style="font-family: 'Comfortaa', cursive; font-size:12px" data-toggle='modal' data-target='#EsqueceuSenha'>Esqueceu a senha?</small>
           
                </a>
              </div>
              <div class="col-6 text-right">
                <a href="index.php?id=cadastro" class="text-light">
                  <small style="font-family: 'Comfortaa', cursive; font-size:12px">Criar nova conta</small>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
    </div>
  <!--FIM - Login-->
  <!-- Core -->
  <script src="../assets/vendor/jquery/jquery.min.js"></script>
  <script src="../assets/vendor/popper/popper.min.js"></script>
  <script src="../assets/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="../assets/vendor/headroom/headroom.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.0.1"></script>
</body>
</html>