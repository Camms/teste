<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Título -->
	<title>Attention Pets</title>
	<link rel="icon" type="imagem/png" href="imagens/logos/icon.png"/>

	<!-- Estilos -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="estilo.css" rel="stylesheet">

</head>
<body id="Topo">
<!--INICIO - Menu-->
<header class="header-global">
    <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
      <div class="container">
          
          <!--Logo-->
        <a class="navbar js-scroll-trigger" href="index.php"href="#Topo">
        <img src="imagens/logos/Logo_Menuu.png" width="240px">
        </a>
          
          <!-- Botão Menu -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        
          <!-- Divisão/Titulo do Botão -->
        <div class="navbar-collapse collapse" id="navbar_global">
          <div class="navbar-collapse-header">
            <div class="row">
              <div class="col-6 collapse-brand">
                <a href="./index.html">
                  <img src="imagens/logos/Logo_Menu.png">
                </a>
              </div>
                
          <!-- Botão Fechar Menu -->  
              <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                  <span></span>
                  <span></span>
                </button>
              </div>
                
            </div>
          </div>
            
            <!-- Botão Menu Dentro -->
          <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
              
              <!-- Inicio -->
            <li class="nav-item dropdown">
              <a href="index.php?id=menu" class="nav-link">
                <span class="nav-link-inner--text">Início</span>
              </a>
              
            </li>

            <!-- Outros -->
            <li class="nav-item dropdown">
              <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
                <span class="nav-link-inner--text">Outros</span>
              </a>
              <div class="dropdown-menu dropdown-menu-xl">
                <div class="dropdown-menu-inner">

                  <a href="#entrevistas" class="media d-flex align-items-center js-scroll-trigger">
                    <div class="icon icon-shape bg-gradient-primary rounded-circle text-white">
                      <i class="fas fa-clipboard-check"></i>
                    </div>
                    <div class="media-body ml-3">
                      <h6 class="heading text-primary mb-md-1">Entrevistas</h6>
                      <p class="description d-none d-md-inline-block mb-0">Algumas pessoas foram selecionadas para testar as funcionalidades do site. Clique e veja o que elas tem a dizer!</p>
                    </div>
                  </a>

                  <a href="#" class="media d-flex align-items-center js-scroll-trigger">
                    <div class="icon icon-shape bg-gradient-success rounded-circle text-white">
                      <i class="fas fa-stream"></i>
                    </div>
                    <div class="media-body ml-3">
                      <h6 class="heading text-success mb-md-1">Desenvolvedora</h6>
                      <p class="description d-none d-md-inline-block mb-0">Deseja entrar em contato com a desenvolvedora para assuntos profissionais? Clique Aqui!</p>
                    </div>
                  </a>

                 

                  <a href="index.php?#contact" class="media d-flex  align-items-center js-scroll-trigger">
                    <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
                      <i class="fas fa-comments"></i>
                    </div>
                    <div class="media-body ml-3">
                      <h5 class="heading text-warning mb-md-1">Fale Conosco</h5>
                      <p class="description d-none d-md-inline-block mb-0">Deseja dar alguma sugestão para melhorias do site? Clique aqui e fale conosco!</p>
                    </div>
                  </a>

           <!-- Cadastrar -->
           <li class="nav-item dropdown">
              <a href="index.php?id=cadastro_pets" class="nav-link">
                <span class="nav-link-inner--text">Cadastrar Pet</span>
              </a>
              
            </li>
            </ul>
             <ul class="navbar-nav align-items-lg-center ml-lg-auto"> 
              <?php
            @session_start();
            if(isset($_SESSION['logado'])){
              ?>
              <!--INICIO - Sair-->
              <li class="nav-item dropdown">
                <a href="index.php?id=logout" class="nav-link" data-toggle="dropdown" href="#" role="button">
                 <span class="nav-link-inner--text">Sair</span>
                </a>
              </li>
              <!--FIM - Sair-->
              <?php
            }else{
              ?>
              <!--INICIO - Entrar-->
			  <li class="nav-item dropdown">
              <a href="index.php?id=login"  class="nav-link">
                
				<span class="btn-inner--icon">
                  <i class="ni ni-curved-next mr-2"></i>
                </span>
				
                <span class="nav-link-inner--text">Entrar</span>
              </a>
              </li>
              <!--FIM - Entrar-->
              <?php
            }
          ?>
              </ul>  
		</div>
        </div>
</nav>
</header>
<!--FIM - Menu-->
<?php
@session_start();
if(isset($_SESSION['logado'])){
$logado=$_SESSION['logado'];
}else {
   $logado=0;
      echo "<script>location.href='index.php'</script>";
}
if($logado==1){
	  echo "<script>location.href='index.php?id=menu'</script>";
}else {
?>
  <?php } ?>        
        
<div class="fundo" style="background-image: url('imagens/carousel/carousel1.png')">
<br><br><br>
<!--INICIO - Cadastro-->
<?php
  if(!isset($_POST['conf'])){
 ?>
    
<div class="login-form">
<form action="index.php?id=dieta" method="post">
<input type="hidden" name="conf" value="1" />
      <div class="container pt-lg-sm">
        <div class="row justify-content-center">
          <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0"> 
              <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                  <small>Preencha os campos abaixo!</small>
                </div>
                <form role="form">
                    
                     <!-- Alimento -->
                  <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-utensils"></i></span>
                      </div>
                        
                      <input type="text" class="form-control" name="alimento" placeholder="Tipo do Alimento" required="required">
                        
                    </div>
                  </div>
                    
                    <!-- Sabor --> 
                  <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-fish"></i></span>
                      </div>
                        
                      <input type="text" class="form-control" name="sabor" placeholder="Sabor do Alimento" required="required">
                        
                    </div>
                  </div>

                  <!-- Marca --> 
                  <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-bone"></i></span>
                      </div>
                        
                      <input type="text" class="form-control" name="marca" placeholder="Marca do Alimento" required="required">
                        
                    </div>
                    </div>
                    
                  <div class="mx-auto" style="width: 200px;">
                   <button type="submit" class="btn btn-primary btn-block">Adicionar Dieta!</button>
                  </div>
                  <li class="nav-item dropdown">
                <a href='index.php?id=menu' class="nav-link">

                  <span class="nav justify-content-center">Cancelar</span>
                </a>
              </li>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
</form>
</div>
    
<?php } else
{
	include_once("funcoes.php");
	$PDO = conectar();
    $alimentoDieta = strip_tags($_POST['alimento']);
	$saborDieta = strip_tags($_POST['sabor']);
	$marcaDieta = strip_tags($_POST['marca']);
	$sql="INSERT INTO dieta (alimento, sabor, marca) VALUES (:alimento, :sabor, :marca)";
	$inserir= $PDO->prepare($sql);
    $inserir->bindParam(":alimento",$alimentoDieta);
	$inserir->bindParam(":sabor",$saborDieta);
	$inserir->bindParam(":marca",$marcaDieta);
	$inserir->execute();
	$tot_reg = $inserir->rowCount();
	if($tot_reg>0)
	{
  	echo "<script>location.href='index.php'</script>";
	}
}
 ?>      
</div>
  <!--FIM - Cadastro-->
  <!-- Core -->
  <script src="../assets/vendor/jquery/jquery.min.js"></script>
  <script src="../assets/vendor/popper/popper.min.js"></script>
  <script src="../assets/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="../assets/vendor/headroom/headroom.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.0.1"></script>
</body>
</html>