<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Título -->
  <title>Attention Pets</title>
  <link rel="icon" type="imagem/png" href="imagens/logos/icon.png" />

  <!-- Estilos -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="estilo.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <style>
    body {
      background-color: #EDEDED;
      font-family: "Lato", sans-serif;
    }

    * {
      /*       border: 1px red solid; */

    }

    body {
      overflow-x: hidden;
    }

    .list-group-item {
      font-family: 'Comfortaa', cursive;
      font-size: 13px;
      text-indent: 12px;
      color: black;
    }

    #sidebar-wrapper {
      margin-left: -15rem;
      -webkit-transition: margin .25s ease-out;
      -moz-transition: margin .25s ease-out;
      -o-transition: margin .25s ease-out;
      transition: margin .25s ease-out;
    }

    #sidebar-wrapper a {
      border-radius: 2px;
      text-decoration: none;
      font-size: 13px;
      display: block;
      transition: 0.3s;
    }

    #sidebar-wrapper .sidebar-heading {
      padding: 0.875rem 1.25rem;
      font-size: 1.2rem;

    }

    #sidebar-wrapper .list-group {
      width: 15rem;
    }

    #page-content-wrapper {
      min-width: 100vw;
    }

    #wrapper.toggled #sidebar-wrapper {
      margin-left: 0;
    }

    .btn-FaleConosco {
      border: 1px solid transparent;
      border-color: #11cdef;
      background-color: #11cdef;
      padding: .375rem .75rem;
      border-radius: .25rem;
      transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
      font-size: .875rem;
    }

    @media (min-width: 1024px) {
      #sidebar-wrapper {
        margin-left: 0;
      }

      #page-content-wrapper {
        min-width: 0;
        width: 100%;
      }

      #wrapper.toggled #sidebar-wrapper {
        margin-left: -15rem;
      }
    }

    .thumbnail {
      overflow: hidden;
    }

    .thumbnail img {
      transition: 0.3s all;
    }

    .card:hover>div.thumbnail img {
      transform: scale(1.1);
      position: relative;

    }

    .card:hover {
      border-radius: 0;
      filter: blur(1px) #000000;
      -webkit-filter: blur(1px);
      filter: drop-shadow(0px 0px 10px #6E4D4C);
    }

    .card {
      border-radius: 13px;
      display: flex;
      flex-direction: column;
      justify-content: center;
      font-size: 20px;
      font-family: 'bebaskai';
      letter-spacing: 2px;
      margin: 34px 15px;
      transition: 0.3s all;
      cursor: pointer;
      box-shadow: 13px 15px 20px -12px black;
    }
  </style>
</head>

<body id="inicio">


  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-bottom: 19px;"><img src="imagens/logos/Logo_Nav_Icon.png" width="200" class="img-fluid"></div>
      <div class="list-group list-group-flush">

        <a href="#inicio" class="list-group-item list-group-item-action bg-light js-scroll-trigger"><img src="imagens/icons/house.svg" class="mb-2 mt-2" width="30"> Início</a>

        <a href="index.php?id=cadastro_admin" class="list-group-item list-group-item-action bg-light js-scroll-trigger"><img src="imagens/icons/user.svg" width="30" class="mb-2 mt-2"> Cadastrar Usuário</a>

       <?php
        @session_start();
        if (isset($_SESSION['logado'])) {
          $logado = $_SESSION['logado'];
          $idUsuario = $_SESSION['idUsuario'];
          $nome = $_SESSION['nome']; 
          ?>
          <!--INICIO - Sair-->
          <a href='index.php?id=logout' class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/logout.svg" class="mb-2 mt-2" width="30"> Sair</a>
          <!--FIM - Sair-->
        <?php
        }
        ?>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">

        <img src="imagens/icons/menu-button.svg" class="mb-2 mt-2" width="30" id="menu-toggle" style="cursor:pointer;"><div class="text-center text-muted mb-4">
                  
                </div> <h5 style="font-size:20px; text-indent:10px;padding-right:30px;margin-top: 10px;"><b>Bem-vindo(a)</b>, <?php echo $nome ?>  <img src="imagens/icons/pet.svg" width="30"></h5>
      </nav>
      <div>
    </div>
  
<!--INICIO - Modal Editar -->
<div class="modal fade" id="EditarUsuario" tabindex="-1" role="dialog" aria-labelledby="EditarUsuarioLabel" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="EditarUsuarioLabel">Editar Usuário</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
<?php
  if(!isset($_POST['conf'])){
 ?>
      <div class="modal-body">
<form method="post">
       <input type="hidden" name="conf" value="1" /> 
        
          <!-- CODIGO PARA EDITAR USUARIO -->
        
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success">Editar</button></form></div>
 <?php } else
{
	include_once("funcoes.php");
	$PDO = conectar();
    $nomeLogin = strip_tags($_POST['nome']);
	$usuarioLogin = strip_tags($_POST['usuario']);
	$emailLogin = strip_tags($_POST['email']);
    $senhaLogin = strip_tags($_POST['senha']);
	$sql="INSERT INTO usuarios (nome, usuario, email, senha) VALUES (:nome, :usuario, :email, :senha)";
	$inserir= $PDO->prepare($sql);
    $inserir->bindParam(":nome",$nomeLogin);
	$inserir->bindParam(":usuario",$usuarioLogin);
	$inserir->bindParam(":email",$emailLogin);
    $inserir->bindParam(":senha",$senhaLogin);  
	$inserir->execute();
	$tot_reg = $inserir->rowCount();
	if($tot_reg>0)
	{
  	echo "<script>location.href='index.php?id=admin'</script>";
	}
  }
 ?>  
          </div>
      </div>
  </div>
</div>
<!--FIM - Modal Editar --> 
        
        
<?php
        @session_start();
        if (isset($_SESSION['logado'])) {
          $logado = $_SESSION['logado'];
          
        } else {
          $logado = 0;
          echo "<script>location.href='index.php'</script>";
        } ?>

        <!--INICIO - Cards-->
        <div class="main" style="background-image:url(imagens/teste3.png); ">

          <?php
          include_once("funcoes.php");
          $PDO = conectar();

          /*******
  Aqui vamos fazer o tratamento do dados e informações para que
  possamos fazer a paginação dos itens na pagina
           *******/
          //Se pg não existe atribui 1 a variável pg
          $itens_por_pagina = 6;
          $pag = (isset($_GET['pag'])) ? (int) $_GET['pag'] : 1;
          $sql1 = "SELECT * FROM usuarios";
          $pesquisa1 = $PDO->prepare($sql1);
          $pesquisa1->execute();
          $tot_reg = $pesquisa1->rowCount();
          $num_paginas = ceil($tot_reg / $itens_por_pagina);
          $inicio = ($pag * $itens_por_pagina) - $itens_por_pagina;
          /****************** FIM ***********************************/
          ?>
          <?php
          $itens_por_pagina = 6;
          $sql = "SELECT * FROM usuarios LIMIT
    $inicio, $itens_por_pagina";
          $sql = "SELECT * FROM usuarios ";
          $pesquisa = $PDO->prepare($sql);
          $pesquisa->execute();
          ?>

          <br>

          <div class="container">
            <div class="row">
              <?php
              $x = 1;
              while ($resultado = $pesquisa->fetch(PDO::FETCH_ASSOC)) {
                $count = $pesquisa1->rowCount($resultado);
                if ($count < 1) {
                  echo "<div class='alert alert-danger'>Não tem nenhum usuário cadastrado!</div>";
                } else {

                ?>

                <div class="col-12 col-sm-12 col-md-4">
                  <div class="card-dog mb-4">
                    <div class="card bd-dark">
                      
                      <div class="card-body">
                        <div class="card-title h5" style="text-align:center">
                          <?php echo $resultado['nome']   ?>
                        </div>
                        <p class="card-text text-dark" style="text-align:center">
                            Usuário: <?php echo $resultado['usuario'] ?></p>
                          
                        <p class="card-text text-dark" style="text-align:center">
                            E-mail: <?php echo $resultado['email'] ?></p>
                          
                        <p class="card-text text-dark" style="text-align:center">
                            Senha: <?php echo $resultado['senha'] ?></p>
                          
                          <?php 
                  if (isset($nomeLogin)) {
                    echo $nomeLogin;
                  } else {
                    echo "<button class='btn btn-sm btn-warning' data-toggle='modal' data-target='#EditarUsuario'>Editar</button>";
                  }
                ?>
                          
                          <?php 
                  if (isset($nomeLogin)) {
                    echo $nomeLogin;
                  } else {
                    echo "<button class='btn btn-sm btn-danger' data-target='#ExcluirUsuario'>Excluir</button>";
                  }
                ?>
                   
                      </div>
                    </div>
                  </div>

                </div>

                </a>

              <?php
                }
                if ($x % 3 == 0) {
                  echo "</div>  <div class='row'>";
                }
                $x++;
              }
              ?>

            </div>
            </div>
          <nav>
            <ul class="pagination justify-content-center">
              <li class="page-item"><a class="page-link" href="index.php?id=admin&pag=1">
                  <</a> </li> <?php for ($i = 1; $i <= $num_paginas; $i++) { ?> <li class="page-item"><a class="page-link" href="index.php?id=admin&pag=<?php echo $i ?>">

                      <?php echo  $i; ?></a></li>
            <?php } ?>
            <li class="page-item"><a class="page-link" href="index.php?id=admin&pag=<?php echo $num_paginas ?>">></a></li>

            </ul>
          </nav><br></div>        
<!--
   <li class='active'><a href='primeiro.php?id=cad_usuario'><span>Cadastro de Usuários</span></a></li><br>
   <li><a href='primeiro.php?id=alt_usuario'><span>Alteração/Exclusão Usuários</span></a></li><br>
   <li><a href='primeiro.php?id=ler_mensagens'><span>Ler Mensagens</span></a></li><br>
   <li><a href='#'><span>Inclusão de Notícias</span></a></li><br>
   <li><a href='#'><span>Alteração/Exclusão Notícias</span></a></li><br>
   <li class='last'><a href='primeiro.php?id=logout'><span>Sair Sistema</span></a></li><br>
-->

              </div>

    <!-- Bootstrap core JavaScript -->
    <script src="assets/jquery/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Menu Toggle Script -->
    <script>
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
    </script>
  </div>
</body>


</html>