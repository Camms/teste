<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Título -->
  <title>Attention Pets</title>
  <link rel="icon" type="imagem/png" href="imagens/logos/icon.png" />

  <!-- Estilos -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="estilo.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <style>
    body {
      background-color: #EDEDED;
      font-family: "Lato", sans-serif;
    }

    * {
      /*       border: 1px red solid; */

    }

    body {
      overflow-x: hidden;
    }

    .list-group-item {
      font-family: 'Comfortaa', cursive;
      font-size: 13px;
      text-indent: 12px;
      color: black;
    }

    #sidebar-wrapper {

      min-height: 200vh;
      margin-left: -15rem;
      -webkit-transition: margin .25s ease-out;
      -moz-transition: margin .25s ease-out;
      -o-transition: margin .25s ease-out;
      transition: margin .25s ease-out;
    }

    #sidebar-wrapper a {
      border-radius: 2px;
      text-decoration: none;
      font-size: 13px;
      display: block;
      transition: 0.3s;
    }

    #sidebar-wrapper .sidebar-heading {
      padding: 0.875rem 1.25rem;
      font-size: 1.2rem;

    }

    #sidebar-wrapper .list-group {
      width: 15rem;
    }

    #page-content-wrapper {
      min-width: 100vw;
    }

    #wrapper.toggled #sidebar-wrapper {
      margin-left: 0;
    }

    .btn-FaleConosco {
      border: 1px solid transparent;
      border-color: #11cdef;
      background-color: #11cdef;
      padding: .375rem .75rem;
      border-radius: .25rem;
      transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
      font-size: .875rem;
    }

    @media (min-width: 1024px) {
      #sidebar-wrapper {
        margin-left: 0;
      }

      #page-content-wrapper {
        min-width: 0;
        width: 100%;
      }

      #wrapper.toggled #sidebar-wrapper {
        margin-left: -15rem;
      }
    }

    .thumbnail {
      overflow: hidden;
    }

    .thumbnail img {
      transition: 0.3s all;
    }

    .card:hover>div.thumbnail img {
      transform: scale(1.1);
      position: relative;

    }

    .card:hover {
      border-radius: 0;
      filter: blur(1px) #000000;
      -webkit-filter: blur(1px);
      filter: drop-shadow(0px 0px 10px #6E4D4C);
    }

    .card {
      border-radius: 13px;
      display: flex;
      flex-direction: column;
      justify-content: center;
      font-size: 20px;
      font-family: 'bebaskai';
      letter-spacing: 2px;
      margin: 34px 15px;
      transition: 0.3s all;
      cursor: pointer;
      box-shadow: 13px 15px 20px -12px black;
    }
  </style>
</head>

<body id="inicio">


  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-bottom: 17px;"><img src="imagens/logos/Logo_Nav_Icon.png" width="200" class="img-fluid"></div>
      <div class="list-group list-group-flush">

        <a href="#inicio" class="list-group-item list-group-item-action bg-light js-scroll-trigger"><img src="imagens/icons/house.svg" class="mb-2 mt-2" width="30"> Início</a>

        <a href="index.php?id=cadastro_pets" class="list-group-item list-group-item-action bg-light js-scroll-trigger"><img src="imagens/icons/dog.svg" width="30" class="mb-2 mt-2"> Cadastrar Pet</a>

        <a href="#faleconosco" class="list-group-item list-group-item-action bg-light js-scroll-trigger"><img src="imagens/icons/network.svg" class="mb-2 mt-2" width="30"> Fale Conosco</a>

        <a href="#mapa" class="list-group-item list-group-item-action bg-light js-scroll-trigger"><img src="imagens/icons/maps-and-flags.svg" class="mb-2 mt-2" width="30"> Mapa</a>

        <?php
        @session_start();
        if (isset($_SESSION['logado'])) {
          ?>
          <!--INICIO - Sair-->
          <a href='index.php?id=logout' class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/logout.svg" class="mb-2 mt-2" width="30"> Sair</a>
          <!--FIM - Sair-->
        <?php
        }
        ?>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">

        <img src="imagens/icons/menu-button.svg" class="mb-2 mt-2" width="30" id="menu-toggle" style="cursor:pointer;">
      </nav>
      <div>

        <?php
        @session_start();
        if (isset($_SESSION['logado'])) {
          $logado = $_SESSION['logado'];
          $idUsuario = $_SESSION['idUsuario'];
        } else {
          $logado = 0;
          echo "<script>location.href='index.php'</script>";
        } ?>

        <!--INICIO - Cards-->
        <div class="main" style="background-image:url(imagens/teste3.png); ">

          <?php
          include_once("funcoes.php");
          $PDO = conectar();

          /*******
  Aqui vamos fazer o tratamento do dados e informações para que
  possamos fazer a paginação dos itens na pagina
           *******/
          //Se pg não existe atribui 1 a variável pg
          $itens_por_pagina = 6;
          $pag = (isset($_GET['pag'])) ? (int) $_GET['pag'] : 1;
          $sql1 = "SELECT * FROM animais WHERE FK_id_usuario = $idUsuario";
          $pesquisa1 = $PDO->prepare($sql1);
          $pesquisa1->execute();
          $tot_reg = $pesquisa1->rowCount();
          $num_paginas = ceil($tot_reg / $itens_por_pagina);
          $inicio = ($pag * $itens_por_pagina) - $itens_por_pagina;
          /****************** FIM ***********************************/
          ?>
          <?php
          $itens_por_pagina = 6;
          $sql = "SELECT * FROM animais LIMIT
          $inicio, $itens_por_pagina";
          $sql = "SELECT * FROM animais WHERE FK_id_usuario = $idUsuario";
          $pesquisa = $PDO->prepare($sql);
          $pesquisa->execute();
          ?>

          <br>

          <div class="container">
            <div class="row">
              <?php
              $x = 1;
              while ($resultado = $pesquisa->fetch(PDO::FETCH_ASSOC)) {
                $count = $pesquisa1->rowCount($resultado);
                if ($count < 1) {
                  echo "<div class='alert alert-danger'>Não tem nenhum animal cadastrado!</div>";
                } else {

                ?>

<?php $id_animal2 = (int)$resultado['id_animal']; ?>

                <div class="col-12 col-sm-12 col-md-4">
                  <div class="card-dog mb-4">
                    <div class="card bd-dark">
                      <div class="thumbnail">
                        <a href="index.php?id=dogdados&id_animal=<?php echo $id_animal2; ?>">

                          <img src=<?php echo "imagens/img_pets/", $resultado['img_pet'] ?> style="height:250px;width:100%;border-radius:13px;" />
                      </div>
                      <div class="card-body">
                        <div class="card-title h5" style="text-align:center">
                        
                        
                          <?php echo $resultado['nome']   ?>
                        </div>
                        <p class="card-text text-dark" style="text-align:center">
                            <?php echo $resultado['fisico'] ?></p>

                          <p class="card-text text-dark" style="text-align:center">
                            <?php
                              if ($resultado['especie'] == "Cachorro") {
                                ?><i class="fas fa-dog"></i> <?php echo $resultado['especie'];
                                                                }
                                                                if ($resultado['especie'] == "Gato") {
                                                                  ?><i class="fas fa-cat"></i> <?php echo $resultado['especie'];
                                                                                    } ?>
                          </p>

                      </div>
                    </div>
                  </div>

                </div>

                </a>

              <?php
                }
                if ($x % 3 == 0) {
                  echo "</div>  <div class='row'>";
                }
                $x++;
              }
              ?>

            </div>
          </div>

          <nav>
            <ul class="pagination justify-content-center">
              <li class="page-item"><a class="page-link" href="index.php?id=menu&pag=1">
                  <</a> </li> <?php for ($i = 1; $i <= $num_paginas; $i++) { ?> <li class="page-item"><a class="page-link" href="index.php?id=menu&pag=<?php echo $i ?>">

                      <?php echo  $i; ?></a></li>
            <?php } ?>
            <li class="page-item"><a class="page-link" href="index.php?id=menu&pag=<?php echo $num_paginas ?>">></a></li>

            </ul>
          </nav>
<br>
        </div>

        <!--INICIO - Parallax -->
        <section id="parallaxBarMenu1" data-speed="6" data-type="background">
          <div class="container-parallax">
            <h2 class="featurette-heading" style="color:#fff">Observação do website</h2>
            <p style="color:#fff; font-size:16px ; font-family: 'Questrial', sans-serif;
"> <img src="imagens/icons/gears.svg" class="ml-3" width="30"> O website está em fase de desenvolvimento, funcionando atualmente como um protótipo.</p>
          </div>

        </section>
        <!--FIM - Parallax -->

        <!--INICIO - Fale Conosco-->
		<section id="contact">
<?php
        if (!isset($_POST['conf'])) {
          ?>
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center"><br><br><br>
						<h2 class="section-heading" style="font-size:60px">Fale Conosco</h2>
						<h3 class="section-subheading text-muted" style="margin-bottom: 20px;">Gostaria de enviar uma mensagem? Há aqui um espaço especial para contatos!</h3>
                        
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">


						 <form method="post">
                          <input type="hidden" name="conf" value="1" />
                           <div class="row">
                            <div class="col-md-6">

                          
                          
                          
                          
                          
                          
                          
                        <!-- Nome -->
                        <div class="form-group">
                          <div class="input-group input-group-alternative mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                            </div>
                            <input type="text" class="form-control" name="nome" placeholder="Nome Completo *" required="required" style="padding: 15px;height: auto;">

                          </div>
                        </div>

                        <!-- Email -->
                        <div class="form-group">
                          <div class="input-group input-group-alternative mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                            </div>
                            <input type="mail" class="form-control" name="email" placeholder="Email *" required="required" style="padding: 15px;height: auto;">

                          </div>
                        </div>

                        <!-- Telefone -->
                        <div class="form-group">
                          <div class="input-group input-group-alternative mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-phone"></i></span>
                            </div>
                            <input type="tel" class="form-control" name="telefone" placeholder="Telefone/Celular *" required="required" style="padding: 15px;height: auto;">

                          </div>
                        </div>
                      </div>


                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="input-group input-group-alternative ">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-pencil-alt"></i></span>
                            </div>
                            <textarea class="form-control" name="mensagem" placeholder="Mensagem *" required="required" style="text-indent: 5px;height:193px"></textarea>

                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12 text-center">
                        <div id="success"></div>
                        <button class="btn btn-info btn-xl" type="submit" style="font-family: 'Lobster', cursive; font-size:22px;padding-right: 30px;padding-left: 30px;padding-top: 15px;padding-bottom: 15px;">Enviar Mensagem</button>
                      </div>


                    </div>
                  </form>
                </div>
              </div>
            </div><br>
            <hr class="featurette-divider">
          </section>
        <?php } else {
          include_once("funcoes.php");
          $PDO = conectar();
          $nomeFaleConosco = strip_tags($_POST['nome']);
          $emailFaleConosco = strip_tags($_POST['email']);
          $telefoneFaleConosco = strip_tags($_POST['telefone']);
          $mensagemFaleConosco = strip_tags($_POST['mensagem']);
          $sql = "INSERT INTO fale_conosco (nome, email, telefone, mensagem) VALUES (:nome, :email, :telefone, :mensagem)";
          $inserir = $PDO->prepare($sql);
          $inserir->bindParam(":nome", $nomeFaleConosco);
          $inserir->bindParam(":email", $emailFaleConosco);
          $inserir->bindParam(":telefone", $telefoneFaleConosco);
          $inserir->bindParam(":mensagem", $mensagemFaleConosco);
          $inserir->execute();
          $tot_reg = $inserir->rowCount();
          if ($tot_reg > 0) {
            echo "<script>location.href='index.php?id=menu'</script>";
          }
        }
        ?>
        <!--FIM - Fale Conosco-->
      </div>

      <!--INICIO - Parallax -->
      <section id="parallaxBarMenu2" data-speed="6" data-type="background">
        <div class="container-parallax">
          <h2 class="featurette-heading" style="color:#fff">Observação do website</h2>
          <p style="color:#fff; font-size:16px ; font-family: 'Questrial', sans-serif;
"> <img src="imagens/icons/gears.svg" class="ml-3" width="30"> O website está em fase de desenvolvimento, funcionando atualmente como um protótipo.</p>
        </div>
      </section>
      <!--FIM - Parallax -->



      <!--INICIO - Prevenções-->
      <section id="mapa" style="background-color:#e9e9dd;">
        <hr class="featurette-divider mt-3">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2 class="section-heading" style="color:#000;font-size:60px">Mapas</h2>
                
                <p style="color:#212529; font-size:16px ; font-family: 'Questrial', sans-serif;
"> O mapa é necessário para indicação de certas localidades e medir distâncias. Aqui, disponibilizamos a você, dois mapas com a localização de Pet Shops, e Clínicas Veterinárias! Clique na localidade que deseja, e veja sua avaliação, comentários, endereço detalhado e as rotas para ir até o local.</p>
                
              <hr class="featurette-divider mt-3 ">
              <!--Pet Shops-->
              <h2 class="featurette-heading"><span style="color:#212529">Pet Shops</span></h2>
            <div class="container">
            </div>
          </div>
        </div>
        <div style="overflow:hidden;">
          <iframe width="2000" height="440" src="https://maps.google.com/maps?q=petshop%20crici%C3%BAma&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
          <div style="width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"></div>
            </div>
            
            
            
            
            
          <style>
            #gmap_canvas img {
              max-width: none !important;
              background: none !important
            }
          </style>
        </div>
        <div class="container">
          <!--Clínicas Veterinárias-->
          <hr class="featurette-divider mt-3">
          <div class="col-lg-12 text-center">
            <h2 class="featurette-heading"><span style="color:#212529">Clínicas Veterinárias</span></h2>
          </div>
        
        <div style="overflow:hidden;">
          <iframe width="2000" height="440" src="https://maps.google.com/maps?q=cl%C3%ADnicas%20veterin%C3%A1rias%20crici%C3%BAma&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
          <div style="width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"></div>
          <hr class="featurette-divider mt-3">
       
</div>

          <style>
            #gmap_canvas img {
              max-width: none !important;
              background: none !important
            }
          </style>
        </div>
      </section>
    </div>
    <!-- Bootstrap core JavaScript -->
    <script src="assets/jquery/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Menu Toggle Script -->
    <script>
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
    </script>
  </div>
  </div>
</body>

<!--FIM - Texto Inicial-->

</html>