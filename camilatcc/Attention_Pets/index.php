<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Título -->
	<title>Attention Pets</title>
	<link rel="icon" type="imagem/png" href="imagens/logos/icon.png"/>

	<!-- Fontes -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lobster&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Comfortaa&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Questrial&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alfa+Slab+One&display=swap" rel="stylesheet">


	<!--Fale Conosco Italico-->
	<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

    
    <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">


	<!-- Estilos -->
	<link href="assets/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="estilo.css" rel="stylesheet">

</head>
<?php
if (isset($_GET['id'])) {
	$id = $_GET['id'];
	if (file_exists("pages/" . $id . ".php")) {
		include("pages/" . $id . ".php");
	}
} else {
	?>

	<body id="Topo">
		<!--INICIO - Menu-->
		<header class="header-global">
			<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
				<div class="container">

					<!--Logo-->
					<a class="navbar js-scroll-trigger" href="#Topo">
						<img src="imagens/icons/pet.svg" width="40"> <img src="imagens/logos/Logo_Nav.png" width="200px">
					</a>

					<!-- Botão Menu -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<!-- Divisão/Titulo do Botão -->
					<div class="navbar-collapse collapse" id="navbar_global">
						<div class="navbar-collapse-header">
							<div class="row">
								<div class="col-10 collapse-brand">
									<a href="./index.html">
										<img src="imagens/icons/pet.svg" width="35"><img src="imagens/logos/Logo_Nav_Resp.png" width="200">
									</a>
								</div>

								<!-- Botão Fechar Menu -->
								<div class="col-2 collapse-close">
									<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
										<span></span>
										<span></span>
									</button>
								</div>

							</div>
						</div>

						<!-- Botão Menu Dentro -->
						<ul class="navbar-nav navbar-nav-hover align-items-lg-center">

							<!-- Fale Conosco -->
							<li class="nav-item dropdown">
								<a href="#contact" class="nav-link js-scroll-trigger" role="button">
									<span class="nav-link-inner--text">Fale Conosco</span>
								</a>
								<div class="dropdown-menu dropdown-menu-lg">
									<div class="dropdown-menu-inner">

										<a href="#contact" class="media d-flex  align-items-center js-scroll-trigger">
											<div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
												<i class="fas fa-comments"></i>
											</div>
											<div class="media-body ml-3">
												<h5 class="text-warning mb-md-1">Fale Conosco</h5>
												<p class="description d-none d-md-inline-block mb-0" style="font-family: 'Comfortaa', cursive; font-size:13px;">Deseja dar alguma sugestão para melhorias do site? Clique aqui e fale conosco!</p>
											</div>
										</a>

									</div>
								</div>
							</li>

							<!-- Outros -->
							<li class="nav-item dropdown">
								<a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
									<span class="nav-link-inner--text">Outros</span>
								</a>
								<div class="dropdown-menu dropdown-menu-xl">
									<div class="dropdown-menu-inner">

										<a href="#entrevistas" class="media d-flex align-items-center js-scroll-trigger">
											<div class="icon icon-shape bg-gradient-primary rounded-circle text-white">
												<i class="fas fa-clipboard-check"></i>
											</div>
											<div class="media-body ml-3">
												<h5 class="text-primary mb-md-1">Entrevistas</h5>
												<p class="description d-none d-md-inline-block mb-0" style="font-family: 'Comfortaa', cursive; font-size:13px;">Algumas pessoas foram selecionadas para testar as funcionalidades do site. Clique e veja o que elas tem a dizer!</p>
											</div>
										</a>

										<a href="#footer" class="media d-flex align-items-center js-scroll-trigger">
											<div class="icon icon-shape bg-gradient-success rounded-circle text-white">
												<i class="fas fa-stream"></i>
											</div>
											<div class="media-body ml-3">
												<h5 class="text-success mb-md-1">Desenvolvedora<h5 class="text-warning mb-md-1"></h5>
													<p class="description d-none d-md-inline-block mb-0" style="font-family: 'Comfortaa', cursive; font-size:13px;">Deseja entrar em contato com a desenvolvedora para assuntos profissionais? Os meios para contato se encontram no fim do site.</p>
											</div>
										</a>

									</div>
								</div>
							</li>
						</ul>
						<ul class="navbar-nav align-items-lg-center ml-lg-auto">
							<?php
								@session_start();
								if (isset($_SESSION['logado'])) {
									?>
								<!--INICIO - Sair-->
              <li class="nav-item dropdown">
                <a href='index.php?id=logout' class="nav-link">
                 <span class="nav-link-inner--text">Sair</span>
                </a>
              </li>
              <!--FIM - Sair-->
              <?php
            }else{
              ?>
              <!--INICIO - Entrar-->
			  <li class="nav-item dropdown">
              <a href="index.php?id=login"  class="nav-link">
				<div class="btn-entrar">
                <span class="nav-link-inner--text"><b>Entrar</b></span>
                </div>
              </a>
              </li>
              <!--FIM - Entrar-->
							<?php
								}
								?>
						</ul>

					</div>
				</div>
			</nav>
		</header>
		<!--FIM - Menu-->
<?php
@session_start();
if(isset($_SESSION['logado'])){
$logado=$_SESSION['logado'];
}else {
   $logado=0;
}
if($logado==1){
	  echo "<script>location.href='index.php?id=menu'</script>";
}else {
?>
  <?php } ?>
		<!--INICIO - Carousel de Imagens-->
		<header>
			<div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner" role="listbox">

					<!-- Primeira Imagem -->
					<div class="carousel-item active" style="background-image: url('imagens/carousel/carousel1.png')">
						<div class="carousel-caption d-md-block" style="
    padding-bottom: 50px;
">
							<div class="container shape-container d-flex align-items-center">

								<div class="col px-0">
									<div class="row align-items-center justify-content-center">
										<div class="col-lg-6 text-center">

											<!-- Logo Site -->

											<img src="imagens/logos/Logo_Index_Icon.png" style="width: 600px;" class="img-fluid">

											<!-- Texto de Apresentação -->
											<p class="lead text-white" style="font-size:20px; font-family: 'Questrial', sans-serif;">Website Desenvolvido Com o Intuíto de Ajudar no Controle Da Saúde e Bem Estar dos Animais Domésticos</p>

											<!-- Botão Cadastre-se -->
											<div class="btn-wrapper mt-5">
												<a href="index.php?id=cadastro" class="btn btn-lg btn-white btn-icon mb-3 mb-sm-0">

													<span class="btn-inner--text" style="font-family: 'Lobster', cursive; font-size:22px">Cadastre-se Agora!</span>
												</a>
											</div>

											<!-- Criador -->
											<div class="mt-5">
												<small class="text-white font-weight-bold" style=" font-family: 'Questrial', sans-serif;">*created by Camila Anastácio</small>
												<img src="imagens/logos/Pata.png" style="height: 20px;">
												<br><br><br>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Segunda Imagem -->
					<div class="carousel-item" style="background-image: url('imagens/carousel/carousel2.png')">
						<div class="carousel-caption d-md-block" style="
    padding-bottom: 50px;
">
							<div class="container shape-container d-flex align-items-center">
								<div class="col px-0">
									<div class="row align-items-center justify-content-center">
										<div class="col-lg-6 text-center">

											<!-- Logo Site -->

											<img src="imagens/logos/Logo_Index_Icon.png" style="width: 600px;" class="img-fluid">

											<!-- Texto de Apresentação -->
											<p class="lead text-white" style="font-size:20px; font-family: 'Questrial', sans-serif;">Website Desenvolvido Com o Intuíto de Ajudar no Controle Da Saúde e Bem Estar dos Animais Domésticos</p>

											<!-- Botão Cadastre-se -->
											<div class="btn-wrapper mt-5">
												<a href="index.php?id=cadastro" class="btn btn-lg btn-white btn-icon mb-3 mb-sm-0">

													<span class="btn-inner--text" style="font-family: 'Lobster', cursive; font-size:22px">Cadastre-se Agora!</span>
												</a>
											</div>

											<!-- Criador -->
											<div class="mt-5">
												<small class="text-white font-weight-bold" style=" font-family: 'Questrial', sans-serif;">*created by Camila Anastácio</small>
												<img src="imagens/logos/Pata.png" style="height: 20px;">
												<br><br><br>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Terceira Imagem -->
					<div class="carousel-item" style="background-image: url('imagens/carousel/carousel3.png')">
						<div class="carousel-caption  d-md-block" style="
    padding-bottom: 50px;
">
							<div class="container shape-container d-flex align-items-center ">
								<div class="col px-0">
									<div class="row align-items-center justify-content-center">
										<div class="col-lg-6 text-center">

											<!-- Logo Site -->

											<img src="imagens/logos/Logo_Index_Icon.png" style="width: 600px;" class="img-fluid">

											<!-- Texto de Apresentação -->
											<p class="lead text-white" style="font-size:20px; font-family: 'Questrial', sans-serif;">Website Desenvolvido Com o Intuíto de Ajudar no Controle Da Saúde e Bem Estar dos Animais Domésticos</p>

											<!-- Botão Cadastre-se -->
											<div class="btn-wrapper mt-5">
												<a href="index.php?id=cadastro" class="btn btn-lg btn-white btn-icon mb-3 mb-sm-0">

													<span class="btn-inner--text" style="font-family: 'Lobster', cursive; font-size:22px">Cadastre-se Agora!</span>
												</a>
											</div>

											<!-- Criador -->
											<div class="mt-5">
												<small class="text-white font-weight-bold" style=" font-family: 'Questrial', sans-serif;">*created by Camila Anastácio</small>
												<img src="imagens/logos/Pata.png" style="height: 20px;">
												<br><br><br>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</header>
		<!--FIM - Carousel de Imagens-->


		<!--INICIO - Texto Inicial-->
		<div class="container marketing" id="Sobre">
			<hr class="featurette-divider">
			<div class="col-md order-md-4">
				<h2 class="featurette-heading"><span class="text-muted">Utilidade do Site</span></h2>
				<p class="lead" style="text-indent: 40px; font-size:18px; font-family: 'Questrial', sans-serif;">Attention Pets é um website que ajuda aos donos de animais a ter um melhor controle da saúde dos pets. Fornecendo todos os dados de cada pet, você terá em mãos a qualquer momento as os dados, vacinas, dieta, mapa de clínicas e entre outros recursos!</p>

				<p class="lead" style="text-indent: 40px; font-size:18px; font-family: 'Questrial', sans-serif;">Criando uma conta no website, os problemas como: esquecer dos cuidados essenciais(água e ração), banho e tosa dos pets, dias de consultas a um médico veterinário, e também, avisos para limpar a área para necessidades dos pets!</p>
			</div>
			<hr class="featurette-divider">
		</div>
		<!--FIM - Texto Inicial-->

		<!--INICIO - Parallax -->
		<section id="parallaxBar1" data-speed="6" data-type="background">
			<div class="container-parallax">
				<h2 style="color:#fff;">Importância das vacinas</h2>
				<p style="color:#fff; font-size:16px ; font-family: 'Questrial', sans-serif;"> <img src="imagens/icons/shield%20(1).svg" class="ml-3" width="30"> A vacinação é o método de proteção mais eficaz </p>

				<p style="color:#fff; font-size:16px ; font-family: 'Questrial', sans-serif;"><img src="imagens/icons/calendar.svg" class="ml-3" width="30"> Manter as vacinas em dias, é uma forma de demonstrar amor e cuidado por seus pets</p>

				<p style="color:#fff; font-size:16px ; font-family: 'Questrial', sans-serif;"><img src="imagens/icons/shield.svg" class="ml-3" width="30"> As vacinas acabam proporcionando uma imunidade sobre as doenças, e cria anticorpos!</p>

				<p style="color:#fff; font-size:16px ; font-family: 'Questrial', sans-serif;"><img src="imagens/icons/cure.svg" class="ml-3" width="30"> Protege os animais de doenças infecciosas causadas por vírus, bactérias e outros microrganismos.</p>

				<p style="color:#fff; font-size:16px ; font-family: 'Questrial', sans-serif;"><img src="imagens/icons/heart.svg" class="ml-3" width="30"> E faz uma enorme diferença na qualidade de vida dos pets, e até mesmo de seus donos!</p>


			</div>
		</section>
		<!--FIM - Parallax -->

		<!--INICIO - Entrevistas-->
		<div class="container marketing" id="entrevistas">
			<hr class="featurette-divider">
			<h2 class="featurette-heading"><span class="text-muted">Entrevistas</span></h2><br>
			<div class="row">
				<div class="col-lg-4">
					<img src="imagens/Entrevista/Entrevista1.jpg" class="rounded mx-auto d-block rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" role="img" aria-label="Placeholder: 140x140">
					<br><br>
					<h5 style="text-align:center;">Marilene Assunção</h5>
					<p class="lead" style="text-indent: 40px; font-size:18px; font-family: 'Questrial', sans-serif;">Utilizei o website por 1 semana, para ver se iria me adaptar as funcionalidades do site em minha rotina, que é muito corrida. E estou sem palavras para descrever o quanto o site, em tão pouco tempo, facilitou minha vida mesmo na correria do dia a dia.</p>
				</div>
				<div class="col-lg-4">
					<img src="imagens/Entrevista/Entrevista.png" class="rounded mx-auto d-block rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" role="img" aria-label="Placeholder: 140x140">
					<br><br>
					<h5 style="text-align:center;">Exemplo Entrevistado</h5>
					<p class="lead" style="text-indent: 40px; font-size:18px; font-family: 'Questrial', sans-serif;">Texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista.</p>
				</div>
				<div class="col-lg-4">
					<img src="imagens/Entrevista/Entrevista.png" class="rounded mx-auto d-block rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" role="img" aria-label="Placeholder: 140x140">
					<br><br>
					<h5 style="text-align:center;">Exemplo Entrevistado</h5>
					<p class="lead" style="text-indent: 40px; font-size:18px; font-family: 'Questrial', sans-serif;">Texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista, texto da entrevista.</p>
				</div>
			</div>
			<hr class="featurette-divider">
		</div>
		<!--FIM - Entrevistas-->

		<!--INICIO - Parallax -->
		<section id="parallaxBar2" data-speed="6" data-type="background">
			<div class="container-parallax">
				<h2 class="featurette-heading" style="color:#fff">Observação do website</h2>
				<p style="color:#fff; font-size:16px ; font-family: 'Questrial', sans-serif;
"> <img src="imagens/icons/gears.svg" class="ml-3" width="30"> O website está em fase de desenvolvimento, funcionando atualmente como um protótipo.</p>
			</div>
		</section>
		<!--FIM - Parallax -->
		<br>
		<!--INICIO - Prevenções-->
		<div class="container">
			<hr class="featurette-divider" id="Prevencoes">
			<h2 class="featurette-heading"><span class="text-muted">Spoiler</span></h2>
			<p class="lead" style="text-indent: 40px; font-size:20px; font-family: 'Questrial', sans-serif;">De um modo geral, abaixo está algumas funções que o site oferece aos seus usuários.</p>
			<div class="row featurette">
				<div class="col-md-7">
					<h2 class="featurette-heading"><span class="text-muted" style="font-size: 26px">Vários Pets</span></h2>
					<p class="lead" style="text-indent: 40px; font-size:19px; font-family: 'Questrial', sans-serif;">É ótimo ver todos os dados dos seus animais <b>organizados</b>, ainda mais quando se têm tudo em um só lugar.</p>
				</div>
				<div class="col-md-5">
					<img src="imagens/spoiler/Screenshot_1.jpg" class="img-fluid rounded mx-auto d-block" width="500" height="1000" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x1000">
				</div>
			</div>

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7 order-md-2">
					<h2 class="featurette-heading"><span class="text-muted" style="font-size: 26px">Informações em mãos</span></h2>
					<p class="lead" style="text-indent: 40px; font-size:19px; font-family: 'Questrial', sans-serif;">Com as informações dos pets <b>sempre em mãos</b>, o site resolve o problema das fixas de vacinas, que na maioria das vezes acabam sendo perdidas.</p>
				</div>
				<div class="col-md-5 order-md-1">
					<img src="imagens/spoiler/Screenshot_2.jpg" class="img-fluid rounded mx-auto d-block" width="500" height="1000" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x1000">
				</div>
			</div>

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7">
					<h2 class="featurette-heading"><span class="text-muted" style="font-size: 26px">Cadastro de Pets</span></h2>
					<p class="lead" style="text-indent: 40px; font-size:19px; font-family: 'Questrial', sans-serif;">Ao cadastrar seu pet, você fornecerá as informações dos seus animais de maneira rápida e prática! Podendo <b>editar</b> sempre que quiser as informações do seu pet e até mesmo <b>excluir</b> alguns pets, para caso de adoção em clínicas veterinárias ou canis legalizados.</p>
				</div>
				<div class="col-md-5">
					<img src="imagens/spoiler/CadastroPets.jpg" class="img-fluid rounded mx-auto d-block" width="500" height="1000" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x1000">
				</div>
			</div>

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7 order-md-2">
					<h2 class="featurette-heading"><span class="text-muted" style="font-size: 26px">Mapa</span></h2>
					<p class="lead" style="text-indent: 40px; font-size:19px; font-family: 'Questrial', sans-serif;">O website disponibiliza um mapa para seus usuários: para casos de emergência em <b>clínicas veterinárias</b> ou compras e cuidados com banho e tosa em <b>pet shops</b>.</p>
				</div>
				<div class="col-md-5 order-md-1">
					<img src="imagens/spoiler/Mapa.jpg" class="img-fluid rounded mx-auto d-block" width="500" height="1000" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x1000">
				</div>
			</div>
			<hr class="featurette-divider">
			<br>
		</div>
		<!--FIM - Prevenções-->



		<!--INICIO - Fale Conosco-->
		<section id="contact">
<?php
        if (!isset($_POST['conf'])) {
          ?>
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center"><br><br><br>
						<h2 class="section-heading" style="font-size:60px">Fale Conosco</h2>
						<h3 class="section-subheading text-muted" style="margin-bottom: 20px;">Gostaria de enviar uma mensagem? Há aqui um espaço especial para contatos!</h3>
                        
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">


						 <form method="post">
                          <input type="hidden" name="conf" value="1" />
                           <div class="row">
                            <div class="col-md-6">



									<div class="form-group">
										<input class="form-control" id="nome" name="nome" type="text" placeholder="Seu Nome *" required="required" data-validation-required-message="Por favor, informe seu nome.">
										<p class="help-block text-danger"></p>
									</div>


									<div class="form-group">
										<input class="form-control" id="email" name="email" type="email" placeholder="Seu Email *" required="required" data-validation-required-message="Por favor, informe seu endereço de email.">
										<p class="help-block text-danger"></p>
									</div>


									<div class="form-group">
										<input class="form-control" id="telefone" name="telefone" type="tel" placeholder="Seu Telefone/Celular *" required="required" data-validation-required-message="Por favor, informe seu número de telefone/celular.">
										<p class="help-block text-danger"></p>
									</div>
								</div>


								<div class="col-md-6">
									<div class="form-group">
										<textarea class="form-control"  id="mensagem" name="mensagem" placeholder="Sua Mensagem *" required="required" data-validation-required-message="Por favor, digite sua mensagem."></textarea>
										<p class="help-block text-danger"></p>
									</div>
								</div>


								<div class="clearfix"></div>
								<div class="col-lg-12 text-center">
									<div id="success"></div>
									<button id="sendMessageButton" name="sendMessageButton" class="btn btn-info btn-xl text-uppercase" type="submit">Enviar Mensagem</button>
                                    <br><br><br><br>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>

		<?php } else {
          include_once("funcoes.php");
          $PDO = conectar();
          $nomeFaleConosco = strip_tags($_POST['nome']);
          $emailFaleConosco = strip_tags($_POST['email']);
          $telefoneFaleConosco = strip_tags($_POST['telefone']);
          $mensagemFaleConosco = strip_tags($_POST['mensagem']);
          $sql = "INSERT INTO fale_conosco (nome, email, telefone, mensagem) VALUES (:nome, :email, :telefone, :mensagem)";
          $inserir = $PDO->prepare($sql);
          $inserir->bindParam(":nome", $nomeFaleConosco);
          $inserir->bindParam(":email", $emailFaleConosco);
          $inserir->bindParam(":telefone", $telefoneFaleConosco);
          $inserir->bindParam(":mensagem", $mensagemFaleConosco);
          $inserir->execute();
          $tot_reg = $inserir->rowCount();
          if ($tot_reg > 0) {
            echo "<script>location.href='index.php'</script>";
          }
        }
        ?>

	<?php } ?>
        
	<!--FIM - Fale Conosco-->

	<!--FOOTER-->
	<footer id="footer">
		<div class="foot">
			<div class="col-md">
				<span class="copyright" style="font-family: 'Comfortaa', cursive; font-size:13px;">Desenvolvido por Camila Anastácio ⋅ 3-51</span>
			</div>

			<div class="col-md">
				<ul class="list-inline social-buttons">
					<li class="list-inline-item">
						<a class="js-scroll-trigger" href="#Topo">
							<i class="fab fa-twitter"></i>
						</a>
					</li>
					<li class="list-inline-item">
						<a class="js-scroll-trigger" href="#Topo">
							<i class="fab fa-whatsapp"></i>
						</a>
					</li>
					<li class="list-inline-item">
						<a class="js-scroll-trigger" href="#Topo">
							<i class="fab fa-facebook-f"></i>
						</a>
					</li>
					<li class="list-inline-item">
						<a class="js-scroll-trigger" href="#Topo">
							<i class="fab fa-instagram"></i>
						</a>
					</li>
					<li class="list-inline-item">
						<a class="js-scroll-trigger" href="#Topo">
							<i class="fab fa-linkedin-in"></i>
						</a>
					</li>
				</ul>
			</div>

			<div class="col-md">
				<ul class="list-inline quicklinks">
					<li class="list-inline-item">
						<a class="js-scroll-trigger" href="#Topo" style="font-family: 'Comfortaa', cursive; font-size:13px;">Privacy Policy</a>
					</li>
					<li class="list-inline-item">
						<a class="js-scroll-trigger" href="#Topo" style="font-family: 'Comfortaa', cursive; font-size:13px;">Terms of Use</a>
					</li>
				</ul>
			</div>
		</div>
	</footer>

	<!-- Bootstrap core JavaScript -->
	<script src="js/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="js/jquery.easing.min.js"></script>

	<!-- Contact form JavaScript -->
	<script src="js/jqBootstrapValidation.js"></script>
	<script src="js/contact_me.js"></script>

	<!-- Custom scripts for this template -->
	<script src="js/agency.min.js"></script>

	<script>
		$('#myModal').on('shown.bs.modal', function() {
			$('#myInput').trigger('focus')
		})
	</script>

	<!-- icones -->

</html>