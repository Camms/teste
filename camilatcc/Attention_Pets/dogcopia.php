<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Título -->
  <title>Attention Pets</title>
  <link rel="icon" type="imagem/png" href="imagens/logos/icon.png" />

  <!-- Estilos -->
  <link href="estilo.css" rel="stylesheet">


  <style>
    body {
      background-color: #EDEDED;
      font-family: "Lato", sans-serif;
    }

    * {
      /*       border: 1px red solid; */

    }

    body {
      overflow-x: hidden;
    }

    .card,
    img {
      border-radius: 13px;
    }

    .list-group-item {
      font-family: 'Comfortaa', cursive;
      font-size: 13px;
      text-indent: 12px;
      color: black;
    }

    #sidebar-wrapper {
      margin-left: -15rem;
      -webkit-transition: margin .25s ease-out;
      -moz-transition: margin .25s ease-out;
      -o-transition: margin .25s ease-out;
      transition: margin .25s ease-out;
    }

    #sidebar-wrapper a {
      border-radius: 2px;
      text-decoration: none;
      font-size: 13px;
      display: block;
      transition: 0.3s;
    }

    #sidebar-wrapper .sidebar-heading {
      padding: 0.875rem 1.25rem;
      font-size: 1.2rem;

    }

    #sidebar-wrapper .list-group {
      width: 15rem;
    }

    #page-content-wrapper {
      min-width: 40vw;
    }

    #wrapper.toggled #sidebar-wrapper {
      margin-left: 0;
    }

    .btn-FaleConosco {
      border: 1px solid transparent;
      border-color: #11cdef;
      background-color: #11cdef;
      padding: .375rem .75rem;
      border-radius: .25rem;
      transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
      font-size: .875rem;
    }

    @media (min-width: 1024px) {
      #sidebar-wrapper {
        margin-left: 0;
      }

      #page-content-wrapper {
        min-width: 0;
        width: 100%;
      }

      #wrapper.toggled #sidebar-wrapper {
        margin-left: -15rem;
      }
    }

    .bg-light {
      background-color: #f8f9fa !important;
    }

    a.bg-light:hover,
    a.bg-light:focus,
    button.bg-light:hover,
    button.bg-light:focus {
      background-color: #dae0e5 !important;
    }

    .card>.list-group:first-child .list-group-item:first-child {
      border-top-left-radius: 0.25rem;
      border-top-right-radius: 0.25rem;
    }

    .card>.list-group:last-child .list-group-item:last-child {
      border-bottom-right-radius: 0.25rem;
      border-bottom-left-radius: 0.25rem;
    }

    .list-group-flush .list-group-item {
      border-right: 0;
      border-left: 0;
      border-radius: 0;
    }

    .list-group-flush:first-child .list-group-item:first-child {
      border-top: 0;
    }

    .list-group-flush:last-child .list-group-item:last-child {
      border-bottom: 0;
    }

    .list-group-item-action {
      width: 100%;
      color: #495057;
      text-align: inherit;
    }

    .list-group-item-action:hover,
    .list-group-item-action:focus {
      color: #495057;
      text-decoration: none;
      background-color: #f8f9fa;
    }

    .list-group-item-action:active {
      color: #212529;
      background-color: #e9ecef;
    }

    .list-group-item {
      position: relative;
      display: block;
      padding: 0.75rem 1.25rem;
      margin-bottom: -1px;
      background-color: #fff;
      border: 1px solid rgba(0, 0, 0, 0.125);
    }

    .list-group-item:first-child {
      border-top-left-radius: 0.25rem;
      border-top-right-radius: 0.25rem;
    }

    .list-group-item:last-child {
      margin-bottom: 0;
      border-bottom-right-radius: 0.25rem;
      border-bottom-left-radius: 0.25rem;
    }

    .list-group-item:hover,
    .list-group-item:focus {
      z-index: 1;
      text-decoration: none;
    }

    .list-group-item.disabled,
    .list-group-item:disabled {
      color: #6c757d;
      background-color: #fff;
    }

    .list-group-item.active {
      z-index: 2;
      color: #fff;
      background-color: #007bff;
      border-color: #007bff;
    }

    .list-group-item-action {
      width: 100%;
      color: #495057;
      text-align: inherit;
    }

    .list-group-item-action:hover,
    .list-group-item-action:focus {
      color: #495057;
      text-decoration: none;
      background-color: #f8f9fa;
    }

    .list-group-item-action:active {
      color: #212529;
      background-color: #e9ecef;
    }

    .card-body {
      width: 500px;
    }
  </style>
</head>

<body id="inicio" style="background-image:url(imagens/teste3.png); ">


  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-bottom: 17px;"><img src="imagens/logos/Logo_Nav_Icon.png" width="200" class="img-fluid"></div>
      <div class="list-group list-group-flush">

        <a href="index.php?id=menu" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/house.svg" class="mb-2 mt-2" width="30"> Início</a>

        <a href="index.php?id=cadastro_pets" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/dog.svg" width="30" class="mb-2 mt-2"> Cadastrar Pet</a>

        <a href="index.php?id=menu#faleconosco" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/network.svg" class="mb-2 mt-2" width="30"> Fale Conosco</a>

        <a href="index.php?id=menu#mapa" class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/maps-and-flags.svg" class="mb-2 mt-2" width="30"> Mapa</a>

        <?php
        @session_start();
        if (isset($_SESSION['logado'])) {
          ?>
          <!--INICIO - Sair-->
          <a href='index.php?id=logout' class="list-group-item list-group-item-action bg-light "><img src="imagens/icons/logout.svg" class="mb-2 mt-2" width="30"> Sair</a>
          <!--FIM - Sair-->
        <?php
        }
        ?>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <img src="imagens/icons/menu-button.svg" class="mb-2 mt-2" width="30" id="menu-toggle" style="cursor:pointer;">
      </nav>

      <!--FIM - Menu-->

      <script>
        function openNav() {
          document.getElementById("mySidenav").style.width = "250px";
          document.getElementById("main").style.marginLeft = "250px";
        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
          document.getElementById("main").style.marginLeft = "0";
        }
      </script>

      <!--INICIO - Modal Adicionar Dieta -->
      <div class="modal fade" id="AddDieta" tabindex="-1" role="dialog" aria-labelledby="AddDietaLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="AddDietaLabel">Adicionar dieta</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php
            if (!isset($_POST['conf'])) {
              ?>
              <div class="modal-body">
                <form method="post">
                  <input type="hidden" name="conf" value="1" />



                  <div class="form-group">
                    <p class="text-muted mb-2">Tipo do alimento</p>
                    <input type="text" class="form-control" name="alimento" placeholder="Exemplo: Ração" required="required">
                  </div>

                  <div class="form-group">
                    <p class="text-muted mb-2">Sabor do alimento</p>
                    <input type="text" class="form-control" name="sabor" placeholder="Exemplo: Carne e Arroz" required="required">
                  </div>

                  <div class="form-group">
                    <p class="text-muted mb-2">Marca</p>
                    <input type="text" class="form-control" name="marca" placeholder="Exemplo: Premier" required="required">
                  </div>




                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-success">Adicionar</button>
                </form>
              </div>
            <?php } else {
              include_once("funcoes.php");
              $PDO = conectar();
              $tipoalimentoDieta = strip_tags($_POST['alimento']);
              $saboralimentoDieta = strip_tags($_POST['sabor']);
              $marcaAlimentoDieta = strip_tags($_POST['marca']);
              $sql = "INSERT INTO dieta (alimento, sabor, marca) VALUES (:alimento, :sabor, :marca)";
              $inserir = $PDO->prepare($sql);
              $inserir->bindParam(":alimento", $tipoalimentoDieta);
              $inserir->bindParam(":sabor", $saboralimentoDieta);
              $inserir->bindParam(":marca", $marcaAlimentoDieta);
              $inserir->execute();
              $tot_reg = $inserir->rowCount();
              if ($tot_reg > 0) {
                echo "<script>location.href='index.php?id=menu'</script>";
              }
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <!--FIM - Modal Adicionar Dieta -->


    <!--INICIO - Modal Adicionar Vacinas -->
    <div class="modal fade" id="AddVacinas" tabindex="-1" role="dialog" aria-labelledby="AddVacinasLabel" aria-hidden="true">
      <div class="modal-dialog " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="AddVacinasLabel">Adicionar vacina</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?php
          if (!isset($_POST['conf'])) {
            ?>
            <div class="modal-body">
              <form method="post">
                <input type="hidden" name="conf" value="1" />



                <div class="form-group">
                  <p class="text-muted mb-2">Nome da vacina</p>
                  <input type="text" class="form-control" name="nome" placeholder="Nome da vacina" required="required">
                </div>

                <div class="form-group">
                  <p class="text-muted mb-2">Data</p>
                  <input type="date" class="form-control" name="datarealizacao" required="required">
                </div>

                <div class="form-group">
                  <p class="text-muted mb-2">Retorno</p>
                  <input type="date" class="form-control" name="dataretorno" required="required">
                </div>




                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  <button type="submit" class="btn btn-success">Adicionar</button>
              </form>
            </div>
          <?php } else {
            include_once("funcoes.php");
            $PDO = conectar();
            $nomeVacina = strip_tags($_POST['nome']);
            $dataVacina = strip_tags($_POST['datarealizacao']);
            $retornoVacina = strip_tags($_POST['dataretorno']);
            $sql = "INSERT INTO vacinas (nome, datarealizacao, dataretorno) VALUES (:nome, :datarealizacao, :dataretorno)";
            $inserir = $PDO->prepare($sql);
            $inserir->bindParam(":nome", $nomeVacina);
            $inserir->bindParam(":datarealizacao", $dataVacina);
            $inserir->bindParam(":dataretorno", $retornoVacina);
            $inserir->execute();
            $tot_reg = $inserir->rowCount();
            if ($tot_reg > 0) {
              echo "<script>location.href='index.php?id=menu'</script>";
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <!--FIM - Modal Adicionar Vacinas -->

  <!--INICIO - Modal Adicionar Exame -->
  <div class="modal fade" id="AddExame" tabindex="-1" role="dialog" aria-labelledby="AddExameLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="AddExameLabel">Adicionar exame</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php
        if (!isset($_POST['conf'])) {
          ?>
          <div class="modal-body">
            <form method="post">
              <input type="hidden" name="conf" value="1" />



              <div class="form-group">
                <p class="text-muted mb-2">Nome do exame</p>
                <input type="text" class="form-control" name="nome" placeholder="Nome do exame realizado" required="required">
              </div>

              <div class="form-group">
                <p class="text-muted mb-2">Retorno</p>
                <div action="dogdados.php" method="POST" enctype="multipart/form-data"><input type="file" required name="img_exame"></div>
              </div>

              <div class="form-group">
                <p class="text-muted mb-2">Data</p>
                <input type="date" class="form-control" name="data" placeholder="data" required="required">
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-success">Adicionar</button>
            </form>
          </div>
        <?php } else {
          include_once("funcoes.php");
          $PDO = conectar();
          $id_animal = $_GET['id_animal'];
          $sql1 = "SELECT * FROM animais WHERE id_animal = $id_animal";
          $nomeExame = strip_tags($_POST['nome']);
          $imgExame = strip_tags($_POST['img_exame']);
          $dataExame = strip_tags($_POST['data']);
          $sql = "INSERT INTO exames (nome, img_exame, data, FK_id_animal) VALUES (:nome, :img_exame, :data, :FK_id_animal)";
          $inserir = $PDO->prepare($sql);
          $inserir->bindParam(":nome", $nomeExame);
          $inserir->bindParam(":img_exame", $imgExame);
          $inserir->bindParam(":data", $dataExame);
          $inserir->bindParam(":FK_id_animal", $id_animal);
          $inserir->execute();
          $tot_reg = $inserir->rowCount();
          if ($tot_reg > 0) {
            echo "<script>location.href='index.php?id=menu'</script>";
          }
        }
        ?>
      </div>
    </div>
  </div>
  </div>
  <!--FIM - Modal Adicionar Exame -->

  <!--INICIO - Dados Pet-->
  <section class="postagens">
    <div class="col-12 col-md-auto mb-4">

      <div class="row">
        <div class="col-sm-12">
          <div class="card shadow-lg">
            <div class="card-title mt-2">
              <?php

              $id_animal = $_GET['id_animal'];
              $nome = $_GET['nome'];
              $img_pet = $_GET['img_pet'];
              ?>


              <h5 class="text-center"><strong style="color: #5193BD; font-size:30px;"><?php
                                                                                      if (isset($tipoalimentoDieta)) {
                                                                                        echo $tipoalimentoDieta;
                                                                                      } else {
                                                                                        echo "<button class='btn btn-sm btn-warning mb-1' data-toggle='modal' data-target='#EditarExame'>Editar</button>";
                                                                                      }

                                                                                      echo $nome; ?> <?php

                              if (isset($tipoalimentoDieta)) {
                                echo $tipoalimentoDieta;
                              } else {
                                echo "<button class='btn btn-sm btn-danger' data-target='#ExcluirUsuario'>Excluir</button>";
                              }
                              ?></strong></h5>

            </div>
            <!--        <img src= "<php echo "imagens/img_pets/", $img_pet['img_pet'] ?>" class="card-image-top" height="200" alt=""/>-->
            <img src="imagens/img_pets/<?php echo $img_pet ?>" class="card-image-top" height="280">

            <div class="overlay d-flex align-items-center justify-content-center">
            </div>


            <div class="card-body">

            <?php ?>
   

              <p class="text-left mb-0 mt-0">raca</p>
              <p class="text-left mb-0 mt-0">peso</p>
              <p class="text-left mb-0 mt-0">físico</p>
              <p class="text-left mb-0 mt-0">espécie</p>
              <!--if(já ta pronto no menu)-->
              <p class="text-left mb-0 mt-0">Castrado</p>
              <!--if(fazer parecido com o de cima)-->
              <p class="text-left mb-0 mt-0">data</p>

            </div>
          </div>
        </div>
      </div>
      <!--FIM - Dados Pet-->


      <!--INICIO - Dieta -->
      <div class="row mt-3">
        <div class="col-md-auto">
          <div class="card shadow-lg">
            <div class="card-title mt-2">
              <h5 class="text-center"><strong style="font-size:25px">Dieta <?php
                                                                            if (isset($tipoalimentoDieta)) {
                                                                              echo $tipoalimentoDieta;
                                                                            } else {
                                                                              echo "<button class='btn btn-sm btn-success' data-toggle='modal' data-target='#AddDieta'>Add</button>";
                                                                            }
                                                                            ?>
                </strong></h5>

            </div>


            <div class="overlay d-flex align-items-center justify-content-center">


              <div class="card-body">
                <div class="table-responsive-lg">
                  <table class="table table-striped ">
                    <thead>
                      <tr>
                        <th scope="col">Alimento</th>
                        <th scope="col">Sabor</th>
                        <th scope="col">Marca</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>

                        <th scope="row">Ração</th>
                        <td>Carne e Arroz</td>
                        <td>Premier Golden</td>



                        <td class="align-items-center justify-content-center">
                          <?php
                          if (isset($tipoalimentoDieta)) {
                            echo $tipoalimentoDieta;
                          } else {
                            echo "<button class='btn btn-sm btn-warning mb-1' data-toggle='modal' data-target='#EditarUsuario'>Editar</button>";
                          }
                          ?>
                          <?php
                          if (isset($tipoalimentoDieta)) {
                            echo $tipoalimentoDieta;
                          } else {
                            echo "<button class='btn btn-sm btn-danger' data-target='#ExcluirUsuario'>Excluir</button>";
                          }
                          ?>

                        </td>
                      </tr>
                      <tr>

                      </tr>


                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--FIM - Dieta -->
    </div>
    <!--INICIO - Vacina -->
    <div class="col-12 col-md-auto">
      <div class="card shadow-lg">
        <div class="card-title mt-2">
          <h5 class="text-center"><strong style="font-size:25px">Exame <?php
                                                                        if (isset($tipoalimentoDieta)) {
                                                                          echo $tipoalimentoDieta;
                                                                        } else {
                                                                          echo "<button class='btn btn-sm btn-success' data-toggle='modal' data-target='#AddExame'>Add</button>";
                                                                        }
                                                                        ?>
            </strong></h5>
        </div>
        <div class="overlay d-flex align-items-center justify-content-center">



          <div class="card-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Nome</th>
                  <th scope="col">Imagem</th>
                  <th scope="col">Data</th>
                  <th></th>


                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">Sangue</th>
                  <td>-/-</td>
                  <!--botão com texto sublinhado - Abrir imagem em um modal-->
                  <td>22/05/2001</td>
                  <td class="align-items-center justify-content-center">
                    <?php
                    if (isset($tipoalimentoDieta)) {
                      echo $tipoalimentoDieta;
                    } else {
                      echo "<button class='btn btn-sm btn-warning mb-1' data-toggle='modal' data-target='#EditarUsuario'>Editar</button>";
                    }
                    ?>
                    <?php
                    if (isset($tipoalimentoDieta)) {
                      echo $tipoalimentoDieta;
                    } else {
                      echo "<button class='btn btn-sm btn-danger' data-target='#ExcluirUsuario'>Excluir</button>";
                    }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>
      </div>
      <!--FIM - Vacinas -->

      <!--INICIO - Exame -->
      <div class="card shadow-lg mt-4">
        <div class="card-title mt-2">
          <h5 class="text-center"><strong style="font-size:25px">Vacinas <?php
                                                                          if (isset($tipoalimentoDieta)) {
                                                                            echo $tipoalimentoDieta;
                                                                          } else {
                                                                            echo "<button class='btn btn-sm btn-success' data-toggle='modal' data-target='#AddVacinas'>Add</button>";
                                                                          }
                                                                          ?>
            </strong></h5>
        </div>
        <div class="overlay d-flex align-items-center justify-content-center">



          <div class="card-body">
            <table class="table table-striped ">
              <thead>
                <tr>
                  <th scope="col">Vacinas</th>
                  <th scope="col">Data</th>
                  <th scope="col">Retorno</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">Cinomose</th>
                  <td>22/05/2001</td>
                  <td>Sem retorno</td>
                  <td class="align-items-center justify-content-center">
                    <?php
                    if (isset($tipoalimentoDieta)) {
                      echo $tipoalimentoDieta;
                    } else {
                      echo "<button class='btn btn-sm btn-warning mb-1' data-toggle='modal' data-target='#EditarExame'>Editar</button>";
                    }
                    ?>
                    <?php
                    if (isset($tipoalimentoDieta)) {
                      echo $tipoalimentoDieta;
                    } else {
                      echo "<button class='btn btn-sm btn-danger' data-target='#ExcluirUsuario'>Excluir</button>";
                    }
                    ?>

                  </td>
                </tr>
                <tr>
                  <th scope="row"></th>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>




        </div>

      </div>
      <div class="card shadow-lg mt-4 mb-4 align-items-center justify-content-center">
        <div class="card-title mt-4 mb-4 ">

          <?php
          if (isset($tipoalimentoDieta)) {
            echo $tipoalimentoDieta;
          } else {
            echo "<button class='btn btn-lg btn-info' data-target='#GerarRelatório'>Gerar Relatório</button>";
          }
          ?>
        </div>
      </div>
      <!--FIM - Exame -->
    </div>
  </section>
  </div>
  </div>
  <!-- Bootstrap core JavaScript -->
  <script src="assets/jquery/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>