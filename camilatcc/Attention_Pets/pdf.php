<?php
    include_once("funcoes.php");
    $PDO = conectar();
    $sql = "SELECT * FROM animais";
    $resultado = $PDO->prepare($sql);
    $resultado->execute();
    while ($dados = $resultado->fetch(PDO::FETCH_ASSOC)) {
	$html = '<table border=1';	
	$html .= '<thead>';
	$html .= '<tr>';
	$html .= '<th>ID</th>';
	$html .= '<th>Raça</th>';
	$html .= '<th>Data de Nascimento</th>';
	$html .= '<th>Peso</th>';
	$html .= '<th>Físico</th>';
    $html .= '<th>Especie</th>';
    $html .= '<th>Castrado</th>';
	$html .= '</tr>';
	$html .= '</thead>';
	$html .= '<tbody>';
	
		$html .= '<tr><td>'.$dados['id_animal'] . "</td>";
		$html .= '<td>'.$dados['raca'] . "</td>";
		$html .= '<td>'.$dados['data'] . "</td>";
		$html .= '<td>'.$dados['peso'] . "</td>";
		$html .= '<td>'.$dados['fisico'] . "</td>";
        $html .= '<td>'.$dados['especie'] . "</td>";
        $html .= '<td>'.$dados['castrado'] . "</td>";
	$html .= '</tbody>';
	$html .= '</table';	 
	}

     

    

	//referenciar o DomPDF com namespace
	use Dompdf\Dompdf;

	// include autoloader
	require_once("dompdf/autoload.inc.php");

	//Criando a Instancia
	$dompdf = new DOMPDF();
	
	// Carrega seu HTML
	$dompdf->load_html('
			<h1 style="text-align: center;">Relatório de Pet</h1>
			'. $html .'
		');

	//Renderizar o html
	$dompdf->render();

	//Exibibir a página
	$dompdf->stream(
		"relatorio_pet.pdf", 
		array(
			"Attachment" => false //Para realizar o download somente alterar para true
		)
	);
?>