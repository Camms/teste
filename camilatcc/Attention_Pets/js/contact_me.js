$(function() {

  $("#contactForm input,#contactForm textarea").jqBootstrapValidation({
    preventSubmit: true,
    submitError: function($form, event, errors) {
      // Mensagem de erro
    },
    submitSuccess: function($form, event) {
      event.preventDefault(); // Impedir comportamento de envio padrão
      // Valores FORM
      var nome = $("input#nome").val();
      var email = $("input#email").val();
      var telefone = $("input#telefone").val();
      var mensagem = $("textarea#mensagem").val();
      var firstName = nome; // Mensagem de Sucesso/Falha
      // Verificar o espaço em branco no nome da mensagem Sucesso / Falha
      if (firstName.indexOf(' ') >= 0) {
        firstName = nome.split(' ').slice(0, -1).join(' ');
      }
      $this = $("#sendMessageButton");
      $this.prop("disabled", true); //Desativar botão de envio até que a chamada AJAX seja concluída para evitar mensagens duplicadas
      $.ajax({
        url: "././mail/contact_me.php",
        type: "POST",
        data: {
          nome: nome,
          telefone: telefone,
          email: email,
          mensagem: mensagem
        },
        cache: false,
          
        success: function() {
          // Successo
          $('#success').html("<div class='alert alert-success'>");
          $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
            .append("</button>");
          $('#success > .alert-success')
            .append("<strong>Sua mensagem foi enviada com sucesso! </strong>");
          $('#success > .alert-success')
            .append('</div>');
          // Limpar todos os campos
          $('#contactForm').trigger("reset");
        },
          
        error: function() {
          // Falha
          $('#success').html("<div class='alert alert-success'>");
          $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
            .append("</button>");
          $('#success > .alert-success').append($("<strong>").text("Sucesso! " + firstName + ", sua mensagem foi enviada!"));
          $('#success > .alert-success').append('</div>');
          // Limpar todos os campos
          $('#contactForm').trigger("reset");
        },
          
          
        complete: function() {
          setTimeout(function() {
            $this.prop("disabled", false); //Reativa o botão enviar quando a chamada AJAX estiver concluída
          }, 1000);
        }
      });
    },
    filter: function() {
      return $(this).is(":visible");
    },
  });

  $("a[data-toggle=\"tab\"]").click(function(e) {
    e.preventDefault();
    $(this).tab("show");
  });
});
$('#nome').focus(function() {
  $('#success').html('');
});
